/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

(function() {
  'use strict';

  class BaseSharingService {
    constructor(
      $q,
      nrpModalService,
      storageServer,
      nrpUser,
      nrpErrorDialog,
      $http,
      bbpConfig
    ) {
      this.storageServer = storageServer;
      this.nrpUser = nrpUser;
      this.nrpErrorDialog = nrpErrorDialog;
      this.$http = $http;
      this.bbpConfig = bbpConfig;
      this.$q = $q;
      this.nrpModalService = nrpModalService;
      this.loadModel();
    }
    loadModel() {
      this.model = {
        sharingUsers: [],
        entitySharingMode: 'private',
        allUsers: [],
        entityModeSharedOption: 'Shared',
        entitySelected: '',
        entityType: ''
      };
      this.storageServer.getAllUsers().then(users =>
        // a list of possible users to share the experiment
        {
          return this.nrpUser.getCurrentUser().then(currentuser => {
            //we exclude the owner of the experiment
            this.model.allUsers = users;
            const index = this.model.allUsers
              .map(e => e.id)
              .indexOf(currentuser.id);
            if (index !== -1) this.model.allUsers.splice(index, 1);
          });
        }
      );
    }
    initScope(scope, entityId, entityType) {
      this.$scope = scope;
      this.$scope.model = this.model;
      this.$scope.search = {
        searchUser: ''
      };
      this.$scope.nrpUser = this.nrpUser;
      this.$scope.nrpErrorDialog = this.nrpErrorDialog;
      this.$scope.$http = this.$http;
      this.$scope.bbpConfig = this.bbpConfig;
      this.$scope.$q = this.$q;
      this.$scope.nrpModalService = this.nrpModalService;
      this.$scope.storageServer = this.storageServer;
      this.$scope.throttle = 150;
      this.$scope.updateSharedEntityMode = this.updateSharedEntityMode;
      this.$scope.selectedUserChange = this.selectedUserChange;
      this.$scope.searchUserChange = this.searchUserChange;
      this.$scope.getSharedEntityMode = this.getSharedEntityMode;
      this.$scope.getSharedEntityUsers = this.getSharedEntityUsers;
      this.$scope.deleteSharedEntityUser = this.deleteSharedEntityUser;
      this.$scope.model.entitySelected = entityId;
      this.$scope.model.entityType = entityType;
    }

    launchSharedEntityWindow(entityId, scope, entityType = null) {
      this.initScope(scope, entityId, entityType);
      this.nrpModalService.createModal({
        templateUrl:
          'scripts/esv/services/sharing-service/entity-sharing.template.html',
        closable: true,
        scope: this.$scope,
        size: 'lg',
        windowClass: 'modal-window'
      });
    }

    searchUserChange(user) {
      this.model.filteredUsers = this.model.allUsers.filter(person =>
        String(person.displayName.toLowerCase()).includes(user.toLowerCase())
      );
    }

    updateSharedEntityMode() {
      throw 'not implemented';
    }

    selectedUserChange() {
      throw 'not implemented';
    }

    getSharedEntityMode() {
      throw 'not implemented';
    }

    getSharedEntityUsers() {
      throw 'not implemented';
    }

    deleteSharedEntityUser() {
      throw 'not implemented';
    }
  }
  window.BaseSharingService = BaseSharingService;
})();
