/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
(function() {
  'use strict';

  /**
   * @module nrp-ui-dialog
   */
  angular.module('nrp-ui-dialog', ['ui.bootstrap.modal']);
  angular.module('nrp-ui-dialog').factory('nrpConfirm', nrpConfirm);
  nrpConfirm.$inject = ['$rootScope', '$uibModal'];

  /**
   * Service to trigger modal dialog.
   *
   * @namespace nrpDialog
   * @memberof module:clb-ui-dialog
   * @param  {object} $rootScope Angular DI
   * @param  {object} $uibModal     Angular DI
   * @return {object}            Service Descriptor
   */
  function nrpConfirm($rootScope, $uibModal) {
    return {
      open: open
    };

    /**
     * Confirmation dialog
     * @param  {object} options Parameters
     * @return {Promise}        Resolve if the confirmation happens, reject otherwise
     */
    function open(options) {
      options = angular.extend(
        {
          scope: $rootScope,
          title: 'Confirm',
          confirmLabel: 'Yes',
          cancelLabel: 'No',
          template: 'Are you sure?',
          closable: true
        },
        options
      );

      var modalScope = options.scope.$new();
      modalScope.title = options.title;
      modalScope.confirmLabel = options.confirmLabel;
      modalScope.cancelLabel = options.cancelLabel;
      modalScope.confirmationContent = options.template;
      modalScope.confirmationContentUrl = options.templateUrl;
      modalScope.closable = options.closable;
      modalScope.securityQuestion = options.securityQuestion;
      modalScope.securityAnswer = options.securityAnswer;

      var instance = $uibModal.open({
        template:
          '<div class="modal-header"><button type="button" ng-show="closable" class="close" ng-click="$dismiss(\'cancel\')" aria-hidden="true">&times;</button><h4 class="modal-title">{{title}}</h4></div><div class="modal-body"><alert ng-if="error" type="danger">{{error.reason}}</alert><div class="alert alert-warning" ng-show="securityQuestion">Unexpected bad things will happen if you don’t read this!</div><ng-include ng-if="confirmationContentUrl" src="confirmationContentUrl"></ng-include><p ng-if="!confirmationContentUrl">{{confirmationContent}}</p><fieldset class="form-group" ng-show="securityQuestion"><label for="securityAnswer">{{securityQuestion}}</label> <input type="text" class="form-control" name="securityAnswer" ng-model="answer"></fieldset></div><div class="modal-footer"><button class="btn btn-default" ng-click="$dismiss(\'cancel\')">{{cancelLabel}}</button> <button class="btn btn-danger" ng-disabled="securityAnswer && answer !== securityAnswer" ng-click="$close()">{{confirmLabel}}</button></div>',
        show: true,
        backdrop: 'static',
        scope: modalScope,
        keyboard: options.keyboard || options.closable
      });
      return instance.result;
    }
  }

  /**
   * @module nrp-error
   */
  angular.module('nrp-error', []);
  angular.module('nrp-error').factory('nrpError', nrpError);
  nrpError.$inject = ['$q'];

  /**
   * @class nrpError
   * @memberof module:nrp-error
   * @desc
   * ``nrpError`` describes a standard error object used
   * to display error message or intropect the situation.
   *
   * A ``nrpError`` instance provides the following properties:
   *
   * * ``type`` a camel case name of the error type.
   * * `message` a human readable message of the error that should
   * be displayed to the end user.
   * * ``data`` any important data that might help the software to
   * inspect the issue and take a recovering action.
   * * ``code`` an error numerical code.
   *
   * The nrpError extends the native Javascript Error instance so it also provides:
   * * ``name`` which is equal to the type
   * * ``stack`` the stack trace of the error (when available)
   *
   * Only ``type``, ``message``, and ``code`` should be considered to be present.
   * They receive default values when not specified by the situation.
   *
   * @param {object} [options] the parameters to use to build the error
   * @param {string} [options.type] the error type (default to ``'UnknownError'``)
   * @param {string} [options.message] the error message (default to ``'An unknown error occurred'``)
   * @param {int} [options.code] the error code (default to ``-1``)
   * @param {object} [options.data] any data that can be useful to deal with the error
   */
  function NrpError(options) {
    options = angular.extend(
      {
        type: 'UnknownError',
        message: 'An unknown error occurred.',
        code: -1
      },
      options
    );
    this.type = options.type || options.name;
    this.name = this.type; // Conform to Error class
    this.message = options.message;
    this.data = options.data;
    this.code = options.code;
    this.stack = new Error().stack;
    if (options instanceof Error) {
      // in case this is a javascript exception, keep the raw cause in data.cause
      this.data = angular.extend({ cause: options }, this.data);
    }
  }
  // Extend the Error prototype
  NrpError.prototype = Object.create(Error.prototype);
  NrpError.prototype.toString = function() {
    return String(this.type) + ':' + this.message;
  };

  /**
   * @namespace nrpError
   * @memberof module:nrp-error
   * @desc
   * ``nrpError`` provides helper functions that all return an
   * ``nrpError`` instance given a context object.
   * @param {object} $q AngularJS injection
   * @return {object} the service singleton
   */
  function nrpError($q) {
    return {
      rejectHttpError: function(err) {
        return $q.reject(httpError(err));
      },
      httpError: httpError,

      /**
       * Build a ``nrpError`` instance from the provided options.
       *
       * - param  {Object} options argument passed to ``nrpError`` constructor
       * - return {nrpError} the resulting error
       * @memberof module:nrp-error.nrpError
       * @param  {object} options [description]
       * @return {object}         [description]
       */
      error: function(options) {
        if (options && options instanceof NrpError) {
          return options;
        }
        return new NrpError(options);
      }
    };

    /**
     * @desc
     * return a `nrpError` instance built from a HTTP response.
     *
     * In an ideal case, the response contains json data with an error object.
     * It also fallback to a reason field and fill default error message for
     * standard HTTP status error.
     * @memberof module:nrp-error.nrpError
     * @param  {HttpResponse} response Angular $http Response object
     * @return {nrpError} a valid nrpError
     */
    function httpError(response) {
      // return argument if it is already an
      // instance of nrpError
      if (response && response instanceof NrpError) {
        return response;
      }

      if (response.status === undefined) {
        return new NrpError({
          message: 'Cannot parse error, invalid format.'
        });
      }
      var error = new NrpError({ code: response.status });

      if (error.code === 0) {
        error.type = 'ClientError';
        error.message = 'The client cannot run the request.';
        return error;
      }
      if (error.code === 404) {
        error.type = 'NotFound';
        error.message = 'Resource not found';
        return error;
      }
      if (error.code === 403) {
        error.type = 'Forbidden';
        error.message =
          'Permission denied: you are not allowed to display ' +
          'the page or perform the operation';
        return error;
      }
      if (error.code === 502) {
        error.type = 'BadGateway';
        error.message = '502 Bad Gateway Error';
        if (response.headers('content-type') === 'text/html') {
          var doc = document.createElement('div');
          doc.innerHTML = response.data;
          var titleNode = doc.getElementsByTagName('title')[0];
          if (titleNode) {
            error.message = titleNode.innerHTML;
          }
        }
        return error;
      }
      if (response.data) {
        var errorSource = response.data;
        if (errorSource.error) {
          errorSource = errorSource.error;
        }
        if (errorSource.type) {
          error.type = errorSource.type;
        }
        if (errorSource.data) {
          error.data = errorSource.data;
        }
        if (errorSource.message) {
          error.message = errorSource.message;
        } else if (errorSource.reason) {
          error.type = 'Error';
          error.message = errorSource.reason;
        }

        if (
          !errorSource.type &&
          !errorSource.data &&
          !errorSource.message &&
          !errorSource.reason
        ) {
          // unkown format, return raw data
          error.data = errorSource;
        }
      }
      return error;
    }
  }

  /**
   * @module nrp-ui-error
   */
  angular.module('nrp-ui-error', ['nrp-error', 'ui.bootstrap']);
  angular.module('nrp-ui-error').factory('nrpErrorDialog', nrpErrorDialog);
  nrpErrorDialog.$inject = ['$uibModal', 'nrpError'];

  /**
   * The factory ``clbUiError`` instantiates modal error dialogs.
   * Notify the user about the given error.
   * @name nrpError
   * @memberof module:clb-ui-error
   * @param  {object} $uibModal Angular DI
   * @param  {object} nrpError  Angular DI
   * @return {object}           Angular Factory
   */
  function nrpErrorDialog($uibModal, nrpError) {
    return {
      open: open
    };

    /**
     * Open an error modal dialog
     * @param  {HBPError} error The error do displays
     * @param  {object} options Any options will be passed to $uibModal.open
     * @return {Promse}         The result of $uibModal.open
     */
    function open(error, options) {
      options = angular.extend(
        {
          template:
            '<div class="error-dialog panel panel-danger"><div class="panel-heading"><h4>{{vm.error.type}}</h4></div><div class="panel-body"><p>{{vm.error.message}}</p></div><div class="panel-footer"><div><button type="button" ng-click="$close(true)" class="btn btn-primary"><span class="glyphicon glyphicon-remove"></span> Close</button> <button type="button" class="btn btn-default pull-right" ng-click="isErrorSourceDisplayed = !isErrorSourceDisplayed">{{isErrorSourceDisplayed ? \'Hide\' : \'Show\'}} scary details <span class="caret"></span></button></div><div uib-collapse="!isErrorSourceDisplayed"><h5>Error Type</h5><pre>{{vm.error.type}}</pre><h6>Error Code</h6><pre>{{vm.error.code}}</pre><h6>Message</h6><pre>{{vm.error.message}}</pre><div ng-if="vm.error.data"><h6>Data</h6><pre>{{vm.error.data}}</pre></div><div><h6>Stack Trace</h6><pre>{{vm.error.stack}}</pre></div></div></div></div>',
          class: 'error-dialog',
          controller: function() {
            var vm = this;
            vm.error = nrpError.error(error);
          },
          controllerAs: 'vm',
          bindToController: true
        },
        options
      );
      return $uibModal.open(options).result.catch(function() {
        // resolve anytime.
        return true;
      });
    }
  }
})();
