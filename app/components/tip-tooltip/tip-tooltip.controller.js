/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
(function() {
  'use strict';

  class TipTooltipController {
    constructor(
      $scope,
      tipTooltipService,
      tipCodesFactory,
      tipClustersFactory,
      DEFAULT_TIP_CLUSTER_IDENTIFIER
    ) {
      this.initialize(
        $scope,
        tipTooltipService,
        tipCodesFactory,
        tipClustersFactory,
        DEFAULT_TIP_CLUSTER_IDENTIFIER
      );
    }

    initialize(
      $scope,
      tipTooltipService,
      tipCodesFactory,
      tipClustersFactory,
      DEFAULT_TIP_CLUSTER_IDENTIFIER
    ) {
      // Indicate to other components that the controller is not ready yet.
      this.ready = false;

      // Store general properties.
      this.$scope = $scope;
      this.tipTooltipService = tipTooltipService;
      this.tipCodesFactory = tipCodesFactory;
      this.tipClustersFactory = tipClustersFactory;
      this.DEFAULT_TIP_CLUSTER_IDENTIFIER = DEFAULT_TIP_CLUSTER_IDENTIFIER;

      // Get the shared tip codes.
      this.$scope.tipCodes = tipCodesFactory.getTipCodes();

      // Get the shared tip clusters.
      this.$scope.tipClusters = tipClustersFactory.getTipClusters();

      this.ready = true;
    }

    /**
     * Show the previous card of the specified tip if possible.
     * @param {Object} tip 
     */
    showPreviousCard(tip) {
      if (tip.tipListPos > 0) {
        tip.tipListPos--;
      }
    }

    /**
     * Show the next card of the specified tip if possible.
     * @param {Object} tip 
     */
    showNextCard(tip) {
      if (tip.tipListPos < tip.tipList.length - 1) {
        tip.tipListPos++;
      }
    }

    /**
     * Hide the specified tip and mark it as read.
     * @param {Object} tip 
     */
    hideTipAndMarkAsRead(tip) {
      // Hide the tip.
      tip.hidden = true;

      // Mark as read.
      tip.markedAsRead = true;

      // Update the local storage if the type is known.
      if (this.tipTooltipService.tipToType(tip)) {
        this.tipTooltipService.setLocalTipPreferences(tip);
      }
    }

    /**
     * Hide all tips of the tip codes list and mark all as read.
     */
    hideAllTips() {
      _.forEach(this.$scope.tipCodes, tip => {
        this.hideTipAndMarkAsRead(tip);
      });
    }

    /**
     * Is the specified tip the first tip that is displayed in this tip cluster.
     * @param {Object} tip 
     */
    isFirstDisplayedInThisTipCluster(tip) {
      if (this.tipTooltipService.displayTip(tip) === false) {
        return false;
      }

      for (
        var i = 0;
        i < this.$scope.tipClusters[this.$scope.clusterIdentifier].tips.length;
        i++
      ) {
        if (
          angular.equals(
            this.$scope.tipClusters[this.$scope.clusterIdentifier].tips[i],
            tip
          )
        ) {
          return true;
        } else if (
          this.tipTooltipService.displayTip(
            this.$scope.tipClusters[this.$scope.clusterIdentifier].tips[i]
          )
        ) {
          return false;
        }
      }
      return false;
    }

    /**
     * Should a "Hide all tips" button be displayed in the specified tip?
     * @param {Object} tip
     */
    displayHideAllTipsButton(tip) {
      let shouldDisplayHideAll = this.$scope.enableHideAll === 'true';
      let isFirstTipInThisCluster = this.isFirstDisplayedInThisTipCluster(tip);

      return shouldDisplayHideAll && isFirstTipInThisCluster;
    }
  }

  TipTooltipController.$inject = [
    '$scope',
    'tipTooltipService',
    'tipCodesFactory',
    'tipClustersFactory',
    'DEFAULT_TIP_CLUSTER_IDENTIFIER'
  ];

  angular
    .module('tipTooltipModule')
    .controller('TipTooltipController', TipTooltipController);
})();
