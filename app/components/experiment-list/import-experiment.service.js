/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
(function() {
  'use strict';
  /* global JSZip: false */
  class ImportExperimentService {
    constructor($q, storageServer, serverError, nrpErrorDialog) {
      this.$q = $q;
      this.storageServer = storageServer;
      this.serverError = serverError;
      this.nrpErrorDialog = nrpErrorDialog;
    }

    createImportErrorPopup(error) {
      this.nrpErrorDialog.open({
        type: 'Import Error.',
        message: error.data
      });
    }

    getImportZipResponses(responses) {
      let importZipResponses = {};
      importZipResponses.numberOfZips = responses.length;
      ['zipBaseFolderName', 'destFolderName'].forEach(name => {
        importZipResponses[name] = responses
          .map(response => response[name])
          .join(', ');
      });
      return importZipResponses;
    }

    getScanStorageResponse(response) {
      let scanStorageResponse = {};
      ['deletedFolders', 'addedFolders'].forEach(name => {
        scanStorageResponse[`${name}Number`] = response[name].length;
        scanStorageResponse[name] = response[name].join(', ');
      });
      return scanStorageResponse;
    }

    scanStorage() {
      return this.storageServer
        .scanStorage()
        .then(this.getScanStorageResponse)
        .catch(this.createImportPopupError);
    }

    zipExperimentFolder(e) {
      let zip = new JSZip();
      let files = e.target.files;
      if (files.length === 0) return; // The folder upload was aborted by user
      let promises = [];
      Array.from(files).forEach(file => {
        promises.push(
          this.$q((resolve, reject) => {
            let reader = new FileReader();
            reader.onerror = err => {
              reader.abort();
              return reject(err);
            };
            reader.onload = f =>
              resolve([file.webkitRelativePath, f.target.result]);
            if (
              file.type.startsWith('image') ||
              file.type === 'application/zip' ||
              file.webkitRelativePath.split('.').pop() === 'h5'
            ) {
              reader.readAsArrayBuffer(file);
            } else {
              reader.readAsText(file);
            }
          })
            .then(([filepath, filecontent]) =>
              this.$q.resolve(
                zip.file(filepath, filecontent, { createFolders: true })
              )
            )
            .catch(err => {
              this.createImportErrorPopup(err);
              return this.$q.reject(err);
            })
        );
      });

      return this.$q
        .all(promises)
        .then(() => zip.generateAsync({ type: 'blob' }))
        .catch(err => {
          this.createImportErrorPopup(err);
          return this.$q.reject(err);
        });
    }

    importExperimentFolder(e) {
      return this.zipExperimentFolder(e).then(zipContent => {
        return this.storageServer
          .importExperiment(zipContent)
          .catch(this.createImportErrorPopup);
      });
    }

    readZippedExperimentExperiment(e) {
      let files = e.target.files;
      let zipFiles = [];
      Array.from(files).forEach(file => {
        if (file.type !== 'application/zip') {
          this.createImportErrorPopup(
            `The file ${file.name} cannot be imported because it is not a zip file.`
          );
        } else {
          zipFiles.push(file);
        }
      });
      let promises = zipFiles.map(zipFile => {
        return this.$q(resolve => {
          let reader = new FileReader();
          reader.onload = f => resolve(f.target.result);
          reader.readAsArrayBuffer(zipFile);
        });
      });
      return this.$q.all(promises);
    }

    importZippedExperiment(e) {
      let promises = this.readZippedExperimentExperiment(e)
        .then(zipContents =>
          zipContents.map(zipContent =>
            this.storageServer.importExperiment(zipContent).catch(err => {
              this.createImportErrorPopup(err);
              return this.$q.reject();
            })
          )
        )
        .then(responses =>
          this.$q
            .all(responses)
            .then(responses => this.getImportZipResponses(responses))
        );
      return promises;
    }
  }

  ImportExperimentService.$$ngIsClass = true;
  ImportExperimentService.$inject = [
    '$q',
    'storageServer',
    'serverError',
    'nrpErrorDialog'
  ];

  angular
    .module('experimentList')
    .service('importExperimentService', ImportExperimentService);
})();
