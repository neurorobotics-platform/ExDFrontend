/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
// eslint-disable-next-line no-unused-vars
/* global THREE: false */
/* global GZ3D: false */

(function() {
  'use strict';

  angular.module('exdFrontendApp.Constants').constant('EDIT_MODE', {
    VIEW: 'view',
    NATURAL: 'natural',
    TRANSLATE: 'translate',
    ROTATE: 'rotate',
    SCALE: 'scale'
  });

  angular.module('exdFrontendApp').directive('environmentDesigner', [
    'STATE',
    'EDIT_MODE',
    'TOOL_CONFIGS',
    'panels',
    'simulationSDFWorld',
    'gz3d',
    'stateService',
    'simulationInfo',
    'nrpErrorDialog',
    'downloadFileService',
    'environmentService',
    'goldenLayoutService',
    '$http',
    'newExperimentProxyService',
    '$q',
    'storageServer',
    'backendInterfaceService',
    '$rootScope',
    'nrpConfirm',
    'nrpModalService',
    'modelsLibrariesService',
    'nrpUser',
    'uploadToKgService',
    function(
      STATE,
      EDIT_MODE,
      TOOL_CONFIGS,
      panels,
      simulationSDFWorld,
      gz3d,
      stateService,
      simulationInfo,
      nrpErrorDialog,
      downloadFileService,
      environmentService,
      goldenLayoutService,
      $http,
      newExperimentProxyService,
      $q,
      storageServer,
      backendInterfaceService,
      $rootScope,
      nrpConfirm,
      nrpModalService,
      modelsLibrariesService,
      nrpUser,
      uploadToKgService
    ) {
      return {
        templateUrl:
          'components/editors/environment-editor/environment-editor.template.html',
        restrict: 'E',
        link: function(scope) {
          scope.stateService = stateService;
          scope.STATE = STATE;

          document.addEventListener('contextmenu', event =>
            event.preventDefault()
          );
          const modelSdfFilename = 'model.sdf';
          var serverConfig = simulationInfo.serverConfig;
          // Used by the view
          scope.assetsPath = serverConfig.gzweb.assets;
          scope.EDIT_MODE = EDIT_MODE;
          scope.gz3d = gz3d;

          scope.isPrivateExperiment = environmentService.isPrivateExperiment();
          scope.isSavingToCollab = false;
          scope.categories = [];
          scope.physicsEngine = simulationInfo.experimentDetails.physicsEngine;
          nrpUser
            .getOwnerDisplayName('me')
            .then(owner => (scope.owner = owner));

          scope.updateVisibleModels = function() {
            scope.visibleModels = [];

            for (var i = 0; i < scope.categories.length; i++) {
              var cat = scope.categories[i];

              if (cat.visible) {
                for (var j = 0; j < cat.models.length; j++) {
                  cat.models[j].color = cat.color['default'];
                  scope.visibleModels.push(cat.models[j]);
                }
              }
            }
          };

          scope.toggleVisibleCategory = function(category) {
            category.visible = !category.visible;
            scope.updateVisibleModels();
          };

          scope.isCategoryVisible = function(category) {
            const categoryFound = scope.categories.find(
              cat => cat.title === category
            );
            return !!categoryFound && categoryFound.visible;
          };

          scope.createErrorPopup = function(errorMessage) {
            nrpErrorDialog.open({
              type: 'Error.',
              message: errorMessage
            });
          };

          scope.uploadModelZip = function(zip, entityType) {
            if (zip.type !== 'application/zip') {
              nrpErrorDialog.open({
                type: 'Error.',
                message:
                  'The file you uploaded is not a zip. Please provide a zipped model'
              });
              return $q.reject();
            }
            return $q(resolve => {
              let textReader = new FileReader();
              textReader.onload = e => resolve([zip.name, e.target.result]);
              textReader.readAsArrayBuffer(zip);
            }).then(([filename, filecontent]) => {
              modelsLibrariesService
                .setCustomModel(filename, entityType, filecontent, false)
                .catch(err => {
                  if (
                    err.data.startsWith(
                      'One of your custom models already has the name'
                    )
                  ) {
                    return nrpConfirm
                      .open({
                        title: err.data,
                        confirmLabel: 'Yes',
                        cancelLabel: 'No',
                        template:
                          'Are you sure you would like to upload the file again?',
                        closable: true
                      })
                      .then(() =>
                        modelsLibrariesService.setCustomModel(
                          filename,
                          entityType,
                          filecontent,
                          true
                        )
                      );
                  } else {
                    nrpModalService.destroyModal();
                    scope.createErrorPopup(err.data);
                    return $q.reject(err);
                  }
                })
                .then(() => scope.regenerateModels())
                .finally(() => (scope.uploadingModel = false));
            });
          };

          scope.uploadModel = function(modelType /*i.e. Robot , Brain*/) {
            var input = $(
              '<input type="file" style="display:none;" accept=".zip">'
            );
            document.body.appendChild(input[0]);
            input.on('change', e =>
              scope.uploadModelZip(e.target.files[0], modelType)
            );
            input.click();
            input.on('change', () => {
              if (
                input[0].files.length &&
                input[0].files[0].type === 'application/zip'
              )
                scope.uploadingModel = true;
            });
            document.body.removeChild(input[0]);
          };

          scope.generateModels = function(modelType) {
            return storageServer.getAllModels(modelType);
          };

          scope.generateModel = function(category) {
            if (category === 'Robots') {
              return scope.generateRobotsModels();
            } else if (category === 'Brains') {
              return scope.generateBrainsModels();
            }
            return $q.reject('Unhandled Category');
          };

          scope.regenerateModels = function() {
            let promises = [];
            let categories = ['Robots', 'Brains'];
            categories.forEach(category => {
              const modelCategory = scope.categories.find(
                cat => cat.title === category
              );
              if (modelCategory) {
                promises.push(
                  scope.generateModel(modelCategory.title).then(res => {
                    modelCategory.models = res;
                    scope.updateVisibleModels();
                  })
                );
              }
            });
            return $q.all(promises);
          };

          scope.generateRobotsModels = function() {
            return scope
              .generateModels('robots')
              .then(robots => {
                robots.forEach(robot => {
                  if (robot.isCustom) {
                    robot.public = false;
                    robot.custom = true;
                  } else {
                    robot.public = true;
                    robot.custom = false;
                  }
                });
                return robots.map(robot => {
                  return {
                    configPath: robot.configPath,
                    modelPath: robot.name,
                    path: robot.path && decodeURIComponent(robot.path),
                    modelSDF: robot.sdf,
                    modelTitle: robot.displayName
                      ? robot.displayName
                      : robot.name,
                    isShared: robot.isShared,
                    thumbnail: robot.thumbnail,
                    custom: robot.isCustom,
                    public: robot.public,
                    isRobot: true,
                    type: robot.type,
                    name: robot.name,
                    description: robot.description
                      ? robot.description
                      : 'Robot has no description'
                  };
                });
              })
              .catch(err =>
                nrpErrorDialog.open({
                  type: 'Model libraries error.',
                  message: `Could not retrieve robots models: \n${err}`
                })
              );
          };

          scope.generateBrainsModels = function() {
            return scope
              .generateModels('brains')
              .then(brains => {
                brains.forEach(brain => {
                  if (brain.isCustom) {
                    brain.public = false;
                    brain.custom = true;
                  } else {
                    brain.public = true;
                    brain.custom = false;
                  }
                });
                return storageServer
                  .getKnowledgeGraphBrains('myQuery')
                  .then(kgBrains => [
                    ...brains,
                    ...kgBrains.map(model => {
                      model.isKnowledgeGraphBrain = true;
                      return model;
                    })
                  ])
                  .catch(err => {
                    nrpErrorDialog.open({
                      type: 'Model libraries error.',
                      message: `Could not retrieve HBP Knowledge Graph brains models: \n${err.status} ${err.statusText}`
                    });
                    return brains;
                  })
                  .then(brains =>
                    brains.map(brain => ({
                      configPath: brain.configPath,
                      path: brain.path && decodeURIComponent(brain.path),
                      modelPath: brain.id,
                      modelTitle: brain.displayName
                        ? brain.displayName
                        : brain.name,
                      thumbnail: brain.thumbnail,
                      custom: brain.isCustom,
                      public: brain.public,
                      isShared: brain.isShared,
                      isBrain: true,
                      urls: brain.urls,
                      isKnowledgeGraphBrain: brain.isKnowledgeGraphBrain,
                      script: brain.script,
                      scriptPath: !brain.isKnowledgeGraphBrain
                        ? brain.scriptPath
                        : brain.urls.fileLoader.split(/[\\/]/).pop(),
                      name: brain.name,
                      description: brain.description
                        ? brain.description
                        : 'Brain has no description',
                      '@id': brain['@id']
                    }))
                  );
              })
              .catch(err =>
                nrpErrorDialog.open({
                  type: 'Model libraries error.',
                  message: `Could not retrieve brains models: \n${err}`
                })
              );
          };

          const modelLibrary = scope.assetsPath + '/' + gz3d.MODEL_LIBRARY;
          $http.get(modelLibrary).then(function(res) {
            scope.categories = res.data;
            scope.updateModelSDF();
            $q
              .all([scope.generateRobotsModels(), scope.generateBrainsModels()])
              .then(([robots, brains]) => {
                let robotCategory = {
                  thumbnail: 'robots.png',
                  title: 'Robots',
                  models: robots
                };
                scope.categories.push(robotCategory);
                GZ3D.modelList.push(robotCategory);
                let brainCategory = {
                  thumbnail: 'brain.png',
                  title: 'Brains',
                  models: brains
                };
                scope.categories.push(brainCategory);
              })
              .finally(() => {
                scope.createModelsCategories();
                scope.updateVisibleModels();
              });
          });

          scope.updateModelSDF = function() {
            for (var i = 0; i < scope.categories.length; i++) {
              var cat = scope.categories[i];
              for (var j = 0; j < cat.models.length; j++) {
                cat.models[j].modelSDF = cat.models[j].modelPath.concat(
                  '/',
                  modelSdfFilename
                );
              }
            }
          };

          scope.createModelsCategories = function() {
            for (var i = 0; i < scope.categories.length; i++) {
              scope.categories[i].models = scope.categories[i].models.filter(
                m => !m.physicsIgnore || m.physicsIgnore !== scope.physicsEngine
              );
              scope.categories[i].visible = i === 0;
              scope.categories[i].colorMode = 'default';
              scope.categories[i].color = {};
              scope.categories[i].color.default =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',95%,87%)';
              scope.categories[i].color.mouseover =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',80%,90%)'; // Mouse over
              scope.categories[i].color.mousedown =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',100%,70%)'; // Mouse down
            }
          };

          scope.setEditMode = function(mode) {
            var setMode = function(m) {
              gz3d.scene.setManipulationMode(m);
              panels.close();
            };

            if (gz3d.scene.manipulationMode === mode) {
              panels.close();
              return;
            } else {
              switch (mode) {
                case EDIT_MODE.VIEW:
                  setMode(mode);
                  break;
                case EDIT_MODE.TRANSLATE:
                case EDIT_MODE.ROTATE:
                  stateService.ensureStateBeforeExecuting(
                    STATE.PAUSED,
                    function() {
                      setMode(mode);
                    }
                  );
                  break;
              }
            }
          };

          scope.onModelMouseDown = (event, model) => {
            event.preventDefault();
            if (model.isBrain) scope.addBrain(model);
            else if (model.isRobot) {
              backendInterfaceService
                .getRobot(model.name)
                .then(() => scope.addRobot(model));
            } else scope.addModel(model);
          };

          scope.addModel = function(model) {
            if (stateService.currentState !== STATE.INITIALIZED) {
              window.guiEvents.emit(
                'spawn_entity_start',
                model.modelPath,
                model.modelSDF
              );
            }
          };

          scope.addRobot = model => {
            if (stateService.currentState !== STATE.INITIALIZED) {
              // manually trigger view mode
              gz3d.scene.setManipulationMode('view');

              gz3d.gui.spawnState = 'START';
              gz3d.scene.spawnModel.start(
                model.path,
                model.path + '/' + model.modelSDF,
                model.modelTitle,
                obj => {
                  let pose = {
                    x: obj.position.x,
                    y: obj.position.y,
                    z: obj.position.z,
                    roll: obj.rotation.x,
                    pitch: obj.rotation.y,
                    yaw: obj.rotation.z
                  };
                  let robotID = obj.name.toLowerCase().replace(/ /gi, '_');
                  let onCreateCallback = model => {
                    if (model.name === robotID) {
                      gz3d.scene.selectEntity(model);
                      goldenLayoutService.openTool(
                        TOOL_CONFIGS.OBJECT_INSPECTOR
                      );
                    }

                    let callbackIndex = gz3d.iface.onCreateEntityCallbacks.indexOf(
                      onCreateCallback
                    );
                    gz3d.iface.onCreateEntityCallbacks.splice(callbackIndex, 1);
                  };
                  gz3d.iface.addOnCreateEntityCallbacks(onCreateCallback);

                  let isCustom = model.custom ? 'True' : 'False';
                  let robotModel = model.name;

                  backendInterfaceService.addRobot(
                    robotID,
                    model.path,
                    robotModel,
                    pose,
                    isCustom
                  );
                }
              );
            }
          };

          function parseBrainError(error) {
            const regex = /transfer\s*Function/gim;
            const matches = regex.exec(error.data.error_message);
            if (matches)
              return 'Some of the transfer functions are referencing variables from the old brain script.\
               Please remove these transfer functions to activate the brain script';
            else return error.data.error_message;
          }

          scope.addBrain = model => {
            // There might not be a brain so we first check the storage
            let brainExistsPromise;
            return storageServer
              .getBrain(simulationInfo.experimentID)
              .then(res => {
                // Ask user to confirm brain replacement if brain already there
                if (res.brain) {
                  brainExistsPromise = nrpConfirm.open({
                    title: 'Replace brain?',
                    confirmLabel: 'Yes',
                    cancelLabel: 'No',
                    template:
                      'Are you sure you would like to change the brain script? \
                If populations in the existing script are referenced in the transfer functions \
                you might have to delete them.',
                    closable: true
                  });
                } else {
                  brainExistsPromise = $q.resolve();
                }
                return (
                  brainExistsPromise
                    // Try to set the brain in the backend
                    .then(() => {
                      let body = {
                        brainType: 'py',
                        dataType: 'text',
                        urls: model.urls,
                        data: model.script,
                        brainPopulations: {}
                      };
                      return (
                        backendInterfaceService
                          .setBrain(body)
                          // If something goes wrong (most likely existing TFs referring to the brain script) open an error dialog
                          .catch(err =>
                            nrpErrorDialog.open({
                              type: 'Error while setting brain.',
                              message: parseBrainError(err)
                            })
                          )
                          // Even if something goes wrong, go ahead and save the brain in the storage as well
                          .finally(() => {
                            backendInterfaceService
                              .getBrain()
                              .then(brainScript => {
                                const lightModel = {
                                  name: model.name,
                                  scriptPath: model.scriptPath,
                                  isKnowledgeGraphBrain:
                                    model.isKnowledgeGraphBrain
                                };
                                return storageServer.saveBrain(
                                  simulationInfo.experimentID,
                                  model.script
                                    ? model.script
                                    : brainScript.data,
                                  {},
                                  true,
                                  lightModel
                                );
                              })
                              .then(() => {
                                goldenLayoutService.openTool(
                                  TOOL_CONFIGS.BRAIN_EDITOR
                                );
                                environmentService.isKnowledgeGraphExperiment =
                                  model.isKnowledgeGraphBrain || false;
                                uploadToKgService.knowledgeGraphBrainId =
                                  model['@id'];
                                $rootScope.$broadcast('BRAIN_SCRIPT_UPDATED');
                              }); // Upon success, open the brain editor
                          })
                      );
                    })
                );
              });
          };

          // When creating a new entity, automatically select the newly created object and open the object inspector.
          scope.onEntityCreated = localEntity => {
            // Current situation: The client has created a local entity. The backend is automatically informed
            // of the changes and will respond with corresponding changes in the updated global scene.
            const MS = 10000;
            let autoSelectCallback;
            let autoSelectTimeout;
            let cleanUp = callback => {
              // Remove the callback from the guiEvents.
              if (callback) {
                gz3d.gui.guiEvents.removeListener(
                  'notification_popup',
                  callback
                );
              }

              clearTimeout(autoSelectTimeout);
            };

            let timeout = new Promise((resolve, reject) => {
              autoSelectTimeout = setTimeout(() => {
                reject('Auto selection timed out in ' + MS + 'ms.');
              }, MS);
            });

            let autoSelect = new Promise(resolve => {
              // Store the name of the local entity to later identify the corresponding global entity.
              let localEntityName = localEntity.name;

              autoSelectCallback = payload => {
                // Current situation: The backend has created a global entity.

                // Select the entity in the up-to-date scene that corresponds to our original local object.
                if (payload === localEntityName + ' inserted') {
                  gz3d.gui.guiEvents.emit('selectEntity', localEntityName);
                  resolve();
                }
              };
              gz3d.gui.guiEvents.on('notification_popup', autoSelectCallback);
            });

            // Open the object inspector window.
            goldenLayoutService.openTool(TOOL_CONFIGS.OBJECT_INSPECTOR);

            // Cleanup after auto selecting the entity or timeout.
            return Promise.race([autoSelect, timeout])
              .then(
                () => {},
                reason => {
                  console.warn(reason);
                }
              )
              .finally(() => {
                cleanUp(autoSelectCallback);
              });
          };
          gz3d.iface.gui.emitter.on('entityCreated', scope.onEntityCreated);

          scope.deleteModel = function() {
            gz3d.gui.guiEvents.emit('delete_entity');
          };

          scope.duplicateModel = function() {
            gz3d.gui.guiEvents.emit('duplicate_entity');
          };

          scope.exportSDFWorld = function() {
            simulationSDFWorld(simulationInfo.serverBaseUrl).export(
              { simId: simulationInfo.simulationID },
              function(data) {
                var linkHref =
                  'data:text/xml;charset=utf-8,' + encodeURIComponent(data.sdf);
                downloadFileService.downloadFile(linkHref, 'world.sdf');
              }
            );
          };

          scope.saveSDFIntoCollabStorage = () => {
            scope.isSavingToCollab = true;

            simulationSDFWorld(simulationInfo.serverBaseUrl).save(
              { simId: simulationInfo.simulationID },
              {},
              () => (scope.isSavingToCollab = false),
              () => {
                nrpErrorDialog.open({
                  type: 'BackendError.',
                  message: 'Error while saving SDF to the Storage.'
                });
                scope.isSavingToCollab = false;
              }
            );
          };
        }
      };
    }
  ]);
})();
