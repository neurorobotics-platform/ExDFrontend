(function() {
  'use strict';

  angular.module('roslibMock', []).service('roslib', [
    function() {
      this.getOrCreateConnectionTo = jasmine.createSpy(
        'getOrCreateConnectionTo'
      );

      let rosLibServiceObject = {
        callService: jasmine.createSpy('callService')
      };

      let rosLibTopicObject = {
        subscribe: jasmine.createSpy('subscribe'),
        unsubscribe: jasmine.createSpy('unsubscribe')
      };

      this.Service = jasmine.createSpy('Service');
      this.ServiceRequest = jasmine.createSpy('ServiceRequest');
      this.createService = jasmine
        .createSpy('createService')
        .and.returnValue(rosLibServiceObject);
      this.createTopic = jasmine
        .createSpy('createTopic')
        .and.returnValue(rosLibTopicObject);
    }
  ]);
})();
