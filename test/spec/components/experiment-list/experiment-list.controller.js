'use strict';

describe('Controller: ExperimentListController', function() {
  let experimentListController;

  let $controller, $rootScope, $scope;
  let nrpModalService;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('experimentList'));

  beforeEach(
    inject(function(_$controller_, _$rootScope_, _nrpModalService_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;

      nrpModalService = _nrpModalService_;
    })
  );

  beforeEach(function() {
    $scope = $rootScope.$new();
    experimentListController = $controller('ExperimentListController', {
      $rootScope: $rootScope,
      $scope: $scope
    });
  });

  it('should be able to create a controller', function() {
    expect(experimentListController).toBeDefined();
  });

  it(' - openMultiExperimentConfig()', function() {
    spyOn(nrpModalService, 'createModal').and.returnValue({ then: () => {} });
    $scope.multiExperimentConfig = undefined;

    $scope.openMultiExperimentConfig();
    expect($scope.multiExperimentConfig).toBeDefined();
    expect(nrpModalService.createModal).toHaveBeenCalled();
  });

  it(' - startMultipleExperiments(), no config defined', function() {
    spyOn($scope.experimentsService, 'startExperiment');
    $scope.multiExperimentConfig = undefined;

    $scope.startMultipleExperiments();
    expect($scope.experimentsService.startExperiment).not.toHaveBeenCalled();
  });

  it(' - startMultipleExperiments(), with config', function() {
    spyOn($scope.experimentsService, 'startExperiment').and.returnValue(
      new Promise(resolve => {
        resolve();
      })
    );
    $scope.multiExperimentConfig = {
      experiment: {
        name: 'my-test-experiment',
        availableServers: ['A', 'B', 'C', 'D']
      },
      numberOfInstances: 3
    };

    $scope.startMultipleExperiments();
    // check startExperiment is called correct amount of times
    expect($scope.experimentsService.startExperiment.calls.count()).toBe(
      $scope.multiExperimentConfig.numberOfInstances
    );
    // check the calls are happening with the correct experiment reference
    $scope.experimentsService.startExperiment.calls.all().forEach(call => {
      expect(call.args[0]).toBe($scope.multiExperimentConfig.experiment);
    });

    // check with number of instances bigger than available servers
    $scope.multiExperimentConfig.launching = false;
    $scope.experimentsService.startExperiment.calls.reset();
    $scope.multiExperimentConfig.numberOfInstances = 10;

    $scope.startMultipleExperiments();
    expect($scope.experimentsService.startExperiment.calls.count()).toBe(
      $scope.multiExperimentConfig.experiment.availableServers.length
    );
  });

  it(' - stopAllSimulations()', function() {
    spyOn($scope, 'stopSimulation');
    let experiment = {
      joinableServers: ['A', 'B', 'C', 'D']
    };

    $scope.stopAllSimulations(experiment);
    // check that stopSimulation has been called the right amount of times
    expect($scope.stopSimulation.calls.count()).toBe(
      experiment.joinableServers.length
    );
    // check that all simulations got a stopSimulation call
    $scope.stopSimulation.calls.all().forEach((call, index) => {
      expect(call.args).toEqual([
        experiment.joinableServers[index],
        experiment
      ]);
    });
  });

  it(' - canStopAllSimulations()', function() {
    spyOn($scope, 'canStopSimulation').and.callThrough();
    $scope.userinfo = {
      userID: 'my-id'
    };
    let experiment = {
      joinableServers: [
        {
          runningSimulation: {
            owner: $scope.userinfo.userID
          }
        },
        {
          runningSimulation: {
            owner: $scope.userinfo.userID
          }
        }
      ]
    };

    // true with all IDs matching
    expect($scope.canStopAllSimulations(experiment)).toBe(true);

    // false with at least one ID being different
    experiment.joinableServers[0].runningSimulation.owner = 'somebody-else';
    expect($scope.canStopAllSimulations(experiment)).toBe(false);
  });
});
