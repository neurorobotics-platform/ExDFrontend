'use strict';

describe('Services: importExperimentService', function() {
  let importExperimentService, $timeout, $q, storageServer, $rootScope;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('experimentList'));
  beforeEach(module('storageServerMock'));

  beforeEach(
    inject(function(
      _importExperimentService_,
      _$timeout_,
      _$q_,
      _storageServer_,
      _$rootScope_
    ) {
      importExperimentService = _importExperimentService_;
      $timeout = _$timeout_;
      $q = _$q_;
      storageServer = _storageServer_;
      $rootScope = _$rootScope_;
    })
  );

  it('should import an experiment folder through the storage server and handle its response', function(
    done
  ) {
    const file = new Blob(['#some python code'], { type: 'text/x-python' });
    const files = [file];
    spyOn(importExperimentService, 'zipExperimentFolder').and.returnValue(
      $q.resolve(file)
    );
    let result = importExperimentService.importExperimentFolder({
      target: { files }
    });
    result
      .then(r => {
        expect(r.destFolderName).toBeDefined();
        expect(r.zipBaseFolderName).toBeDefined();
      })
      .finally(done);
    $timeout.flush();
  });

  it('should display any error caused by importing an experiment folder', function(
    done
  ) {
    spyOn(importExperimentService, 'zipExperimentFolder').and.returnValue(
      $q.resolve()
    );
    storageServer.importExperiment = jasmine
      .createSpy()
      .and.returnValue($q.reject('Damn it, a Storage Server error!'));
    spyOn(importExperimentService, 'createImportErrorPopup');
    let result = importExperimentService.importExperimentFolder();
    result
      .then(() => {
        expect(
          importExperimentService.createImportErrorPopup
        ).toHaveBeenCalled();
      })
      .finally(done);
    $timeout.flush();
  });

  it('should import a zip experiment through the storage server and handle its response', function(
    done
  ) {
    const file = new Blob(['#some python code'], { type: 'text/x-python' });
    const files = [file, file, file];
    spyOn(
      importExperimentService,
      'readZippedExperimentExperiment'
    ).and.returnValue($q.resolve(files));
    let result = importExperimentService.importZippedExperiment({
      target: { files }
    });
    result
      .then(r => {
        expect(r.numberOfZips).toBe(3);
        expect(r.destFolderName.split(',').length).toBe(3);
        expect(r.zipBaseFolderName.split(',').length).toBe(3);
      })
      .finally(done);
    $timeout.flush();
  });

  it('should display any error caused by importing a zip experiment', function(
    done
  ) {
    storageServer.importExperiment = jasmine
      .createSpy()
      .and.returnValue($q.reject('Damn it, a Storage Server error!'));
    spyOn(importExperimentService, 'createImportErrorPopup');
    spyOn(
      importExperimentService,
      'readZippedExperimentExperiment'
    ).and.returnValue($q.resolve([new Blob()]));
    let result = importExperimentService.importZippedExperiment();
    result
      .then(() => {
        expect(
          importExperimentService.createImportErrorPopup.calls.count()
        ).toBe(1);
      })
      .finally(done);
    $timeout.flush();
  });

  it('should scan user storage server and handle the response of the storage server', function(
    done
  ) {
    let result = importExperimentService.scanStorage();
    result
      .then(r => {
        expect(r.deletedFoldersNumber).toBe(3);
        expect(r.addedFoldersNumber).toBe(3);
        expect(r.deletedFolders.split(',').length).toBe(3);
        expect(r.addedFolders.split(',').length).toBe(3);
      })
      .finally(done);
    $timeout.flush();
  });

  it('should display any error caused by scanning the user storage', function(
    done
  ) {
    storageServer.scanStorage = jasmine
      .createSpy()
      .and.returnValue(
        $q.reject('Damn it, a Storage Server error while scanning!')
      );
    spyOn(importExperimentService, 'createImportErrorPopup');
    let result = importExperimentService.scanStorage();
    result
      .then(() => {
        expect(
          importExperimentService.createImportErrorPopup.calls.count()
        ).toBe(1);
      })
      .finally(done);
    $timeout.flush();
  });

  it('should open an error dialog if the uploaded file is not a zip file', function(
    done
  ) {
    spyOn(importExperimentService, 'createImportErrorPopup');
    const file = new Blob(['#some python code'], { type: 'text/x-python' });
    let result = importExperimentService.readZippedExperimentExperiment({
      target: { files: [file] }
    });
    result
      .then(() => {
        expect(
          importExperimentService.createImportErrorPopup
        ).toHaveBeenCalled();
      })
      .finally(done);
    $timeout.flush();
    $rootScope.$digest();
  });

  it('should read a zip experiment', function(done) {
    let fileReaderMocks = [];
    for (let i = 0; i < 3; i++)
      fileReaderMocks.push({ readAsArrayBuffer: angular.noop });
    spyOn(window, 'FileReader').and.returnValues(
      fileReaderMocks[0],
      fileReaderMocks[1],
      fileReaderMocks[2]
    );
    const file = new Blob(['Believe it or not, This is a zip file content'], {
      type: 'application/zip'
    });
    let result = importExperimentService.readZippedExperimentExperiment({
      target: { files: [file, file, file] }
    });
    for (let i = 0; i < 3; i++)
      fileReaderMocks[i].onload({ target: { result: `content${i}` } });
    result
      .then(r => {
        expect(r).toEqual(['content0', 'content1', 'content2']);
      })
      .finally(done);
    $timeout.flush();
    $rootScope.$digest();
  });

  it('should zip an experiment folder', function(done) {
    let fileReaderMocks = [];
    for (let i = 0; i < 3; i++)
      fileReaderMocks.push({
        readAsArrayBuffer: jasmine.createSpy('readArrayAsBuffer'),
        readAsText: jasmine.createSpy('readAsText')
      });
    spyOn(window, 'FileReader').and.returnValues(
      fileReaderMocks[0],
      fileReaderMocks[1],
      fileReaderMocks[2]
    );
    spyOn(window, 'JSZip').and.returnValue({
      file: jasmine.createSpy('file'),
      generateAsync: jasmine
        .createSpy('generateAsync')
        .and.returnValue('generated zip')
    });
    const file1 = {
      webkitRelativePath: 'p3dx/thumbnail.png',
      type: 'image/png'
    };
    const file2 = {
      webkitRelativePath: 'p3dx/record_all_neurons.py',
      type: 'text/python'
    };
    const file3 = {
      webkitRelativePath: 'p3dx/robot.zip',
      type: 'application/zip'
    };
    let result = importExperimentService.zipExperimentFolder({
      target: { files: [file1, file2, file3] }
    });
    for (let i = 0; i < 3; i++)
      fileReaderMocks[i].onload({ target: { result: `content${i}` } });
    result
      .then(r => {
        expect(r).toEqual('generated zip');
        expect(fileReaderMocks[0].readAsArrayBuffer).toHaveBeenCalled();
        expect(fileReaderMocks[1].readAsText).toHaveBeenCalled();
        expect(fileReaderMocks[2].readAsArrayBuffer).toHaveBeenCalled();
      })
      .finally(done);
    $timeout.flush();
    $rootScope.$digest();
  });

  it('should open an error dialog if zipping the experiment folder failed', function(
    done
  ) {
    spyOn(importExperimentService, 'createImportErrorPopup');
    let fileReaderMock = {
      readAsArrayBuffer: jasmine.createSpy('readArrayAsBuffer'),
      readAsText: jasmine.createSpy('readAsText'),
      abort: jasmine.createSpy('abort')
    };
    spyOn(window, 'FileReader').and.returnValue(fileReaderMock);
    spyOn(window, 'JSZip').and.returnValue({
      file: jasmine.createSpy('file'),
      generateAsync: jasmine
        .createSpy('generateAsync')
        .and.returnValue('generated zip')
    });
    const file = {
      webkitRelativePath: 'p3dx/thumbnail.png',
      type: 'image/png'
    };
    let result = importExperimentService.zipExperimentFolder({
      target: { files: [file] }
    });
    fileReaderMock.onerror('Reader error');
    result
      .then(() => {
        expect(
          importExperimentService.createImportErrorPopup
        ).toHaveBeenCalled();
        expect(fileReaderMock.abort).toHaveBeenCalled();
      })
      .finally(done);
    $timeout.flush();
    $rootScope.$digest();
  });
});
