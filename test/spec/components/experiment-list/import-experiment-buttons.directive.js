(function() {
  'use strict';

  describe('Directive: import-experiment-buttons', function() {
    beforeEach(module('experimentList'));
    beforeEach(module('exdFrontendApp'));

    let $scope, element;
    beforeEach(module('importExperimentServiceMock'));
    beforeEach(
      inject(function($rootScope, $compile) {
        element = $compile(
          '<import-experiment-buttons></import-experiment-buttons>'
        )($rootScope);
        $rootScope.$digest();
        $scope = element.scope();
        $scope.selectExperiment = jasmine.createSpy('selectExperiment');
        $scope.loadExperiments = jasmine.createSpy('loadExperiments');
      })
    );

    it('should enable the import buttons and the scan storage button when initializing', function() {
      expect($scope.isImporting).toBe(false);
    });

    it('should clear popup messages when clicking on the close buttons', function() {
      [
        'importFolderResponse',
        'importZipResponses',
        'scanStorageResponse'
      ].forEach(response => ($scope[response] = {}));
      $scope.importFolderPopupClick();
      $scope.importZipPopupClick();
      $scope.scanStoragePopupClick();
      [
        'importFolderResponse',
        'importZipResponses',
        'scanStorageResponse'
      ].forEach(response => expect($scope[response]).toBeUndefined());
    });

    it('should fill $scope.importFolderResponse when clicking on the import folder button', function() {
      let importExperimentFolderInput = element.find(
        '#import-experiment-folder-input'
      );
      expect($scope.importFolderResponse).toBeUndefined();
      importExperimentFolderInput.trigger($.Event('change', {}));
      expect($scope.importFolderResponse).toBeDefined();
      expect($scope.selectExperiment).toHaveBeenCalledWith({ id: 'p3dx_5' });
      expect(importExperimentFolderInput[0].value).toBe('');
      expect($scope.isImporting).toBe(false);
    });

    it('should fill $scope.importZipResponses when clicking on the import zip button', function() {
      let importZippedExperimentInput = element.find(
        '#import-zip-experiment-input'
      );
      expect($scope.importZipResponses).toBeUndefined();
      importZippedExperimentInput.trigger($.Event('change', {}));
      expect($scope.importZipResponses).toBeDefined();
      expect($scope.selectExperiment).toHaveBeenCalledWith({ id: 'p3dx_6' });
      expect(importZippedExperimentInput[0].value).toBe('');
      expect($scope.isImporting).toBe(false);
    });

    it('should fill $scope.scanStorageResponse when clicking on the import zip button', function() {
      expect($scope.scanStorageResponse).toBeUndefined();
      $scope.scanStorageClick();
      expect($scope.scanStorageResponse).toBeDefined();
      expect($scope.selectExperiment).toHaveBeenCalledWith({ id: 'lauron_0' });
      expect($scope.isImporting).toBe(false);
    });
  });
})();
