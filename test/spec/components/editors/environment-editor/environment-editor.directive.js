'use strict';

describe('Directive: environment-designer', function() {
  var rootScope,
    $scope,
    element,
    stateService,
    panels,
    simulationSDFWorld,
    simulationInfo,
    nrpErrorDialog,
    goldenLayoutService,
    httpBackend,
    storageServer,
    $q,
    nrpConfirm,
    backendInterfaceService;

  let modelLibraryMock;
  let fakeCreateEntityFromBackend;

  let backendInterfaceServiceMock = {
    addRobot: jasmine.createSpy('addRobot').and.returnValue(Promise.resolve()),
    setBrain: jasmine.createSpy('setBrain').and.returnValue(Promise.resolve()),
    getBrain: jasmine.createSpy('setBrain').and.returnValue(Promise.resolve())
  };

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('storageServerMock'));
  beforeEach(module('exd.templates'));
  beforeEach(module('currentStateMockFactory'));
  beforeEach(module('simulationInfoMock'));
  beforeEach(module('stateServiceMock'));
  beforeEach(module('dynamicViewOverlayServiceMock'));
  beforeEach(module('goldenLayoutServiceMock'));
  beforeEach(module('gz3dMock'));
  beforeEach(module('sceneInfoMock'));
  beforeEach(
    module(function($provide) {
      $provide.value(
        'simulationSDFWorld',
        jasmine.createSpy('simulationSDFWorld').and.callThrough()
      );
      $provide.value('bbpConfig', {
        get: jasmine.createSpy('get').and.returnValue({
          bbpce016: {
            gzweb: {
              assets: 'mock_assets',
              websocket: 'mock_websocket'
            }
          }
        })
      });
      $provide.value('panels', {
        close: jasmine.createSpy('close')
      });
      $provide.value('backendInterfaceService', backendInterfaceServiceMock);
    })
  );

  beforeEach(
    inject(function(
      $rootScope,
      $compile,
      $document,
      EDIT_MODE,
      STATE,
      TOOL_CONFIGS,
      _currentStateMockFactory_,
      _stateService_,
      _objectInspectorService_,
      _panels_,
      _simulationSDFWorld_,
      _simulationInfo_,
      _nrpErrorDialog_,
      _goldenLayoutService_,
      _$httpBackend_,
      _storageServer_,
      _$q_,
      _nrpConfirm_,
      _backendInterfaceService_
    ) {
      rootScope = $rootScope;
      $scope = $rootScope.$new();
      $scope.EDIT_MODE = EDIT_MODE;
      $scope.STATE = STATE;
      $scope.TOOL_CONFIGS = TOOL_CONFIGS;
      stateService = _stateService_;
      simulationInfo = _simulationInfo_;
      panels = _panels_;
      simulationSDFWorld = _simulationSDFWorld_;
      nrpErrorDialog = _nrpErrorDialog_;
      goldenLayoutService = _goldenLayoutService_;
      httpBackend = _$httpBackend_;
      storageServer = _storageServer_;
      $q = _$q_;
      nrpConfirm = _nrpConfirm_;
      backendInterfaceService = _backendInterfaceService_;

      modelLibraryMock = [
        {
          title: 'Shapes',
          thumbnail: 'shapes.png',
          models: [
            {
              modelPath: 'box',
              modelTitle: 'Box',
              thumbnail: 'img/esv/objects/box.png'
            },
            {
              modelPath: 'sphere',
              modelTitle: 'Sphere',
              thumbnail: 'img/esv/objects/sphere.png'
            },
            {
              modelPath: 'cylinder',
              modelTitle: 'Cylinder',
              thumbnail: 'img/esv/objects/cylinder.png',
              physicsIgnore: 'opensim'
            }
          ],
          color: { default: 'defaultColour' }
        },
        {
          title: 'Lights',
          thumbnail: 'lights.png',
          models: [
            {
              modelPath: 'pointlight',
              modelTitle: 'Point Light',
              thumbnail: 'img/esv/objects/pointlight.png'
            },
            {
              modelPath: 'spotlight',
              modelTitle: 'Spot Light',
              thumbnail: 'img/esv/objects/spotlight.png'
            },
            {
              modelPath: 'directionallight',
              modelTitle: 'Directional Light',
              thumbnail: 'img/esv/objects/directionallight.png',
              physicsIgnore: 'bullet'
            }
          ],
          color: { default: 'defaultColour' }
        }
      ];
      httpBackend.whenGET(/.*assets.*/).respond(modelLibraryMock);

      spyOn(document, 'addEventListener');
      element = $compile('<environment-designer />')($scope);
      window.GZ3D.modelList = [];
      $scope.$digest();
      httpBackend.flush();

      fakeCreateEntityFromBackend = name => {
        $scope.gz3d.gui.guiEvents
          .listeners('notification_popup')
          .forEach(function(callback) {
            callback(name + ' inserted');
          });
      };
    })
  );

  let originalTimeout;

  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 11000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it('should emit "duplicate_entity" on duplicate', function() {
    spyOn($scope.gz3d.gui.guiEvents, 'emit');
    $scope.duplicateModel();
    expect($scope.gz3d.gui.guiEvents.emit).toHaveBeenCalledWith(
      'duplicate_entity'
    );
  });

  it('should initialize scope variables correctly', function() {
    expect($scope.assetsPath).toBeDefined();
  });

  it('should call correctly close panels when edit mode already set', function() {
    stateService.currentState = $scope.STATE.STARTED;
    $scope.gz3d.scene.manipulationMode = $scope.EDIT_MODE.TRANSLATE;
    $scope.setEditMode($scope.EDIT_MODE.TRANSLATE);
    expect(panels.close).toHaveBeenCalled();
  });

  it('should correctly set the edit mode', function() {
    stateService.currentState = $scope.STATE.STARTED;
    $scope.setEditMode($scope.EDIT_MODE.TRANSLATE);
    expect(stateService.ensureStateBeforeExecuting).toHaveBeenCalled();
    expect($scope.gz3d.scene.setManipulationMode).toHaveBeenCalledWith(
      $scope.EDIT_MODE.TRANSLATE
    );
  });

  it('should pause the simulation when needed', function() {
    stateService.currentState = $scope.STATE.STARTED;
    $scope.setEditMode($scope.EDIT_MODE.TRANSLATE);
    expect(
      stateService.ensureStateBeforeExecuting.calls.mostRecent().args[0]
    ).toBe($scope.STATE.PAUSED);
    stateService.currentState = $scope.STATE.STARTED;
    $scope.setEditMode($scope.EDIT_MODE.ROTATE);
    expect(
      stateService.ensureStateBeforeExecuting.calls.mostRecent().args[0]
    ).toBe($scope.STATE.PAUSED);
  });

  it('should not update the state if already in the correct state', function() {
    expect(stateService.setCurrentState.calls.count()).toBe(0);
    stateService.currentState = $scope.STATE.STARTED;
    $scope.setEditMode($scope.EDIT_MODE.VIEW);
    expect(stateService.setCurrentState.calls.count()).toBe(0);
    stateService.currentState = $scope.STATE.PAUSED;
    $scope.setEditMode($scope.EDIT_MODE.TRANSLATE);
    expect(stateService.setCurrentState.calls.count()).toBe(0);
    stateService.currentState = $scope.STATE.PAUSED;
    $scope.setEditMode($scope.EDIT_MODE.ROTATE);
    expect(stateService.setCurrentState.calls.count()).toBe(0);
  });

  it('should call the right REST API for the SDF export process', function() {
    var exportSpy = jasmine.createSpy('export');
    simulationSDFWorld.and.callFake(function() {
      return {
        export: exportSpy.and.callFake(function() {})
      };
    });

    $scope.exportSDFWorld();
    expect(simulationSDFWorld).toHaveBeenCalled();
    expect(exportSpy).toHaveBeenCalled();
  });

  it('should call correctly addModel("box")', function() {
    spyOn($scope, 'addModel');
    $scope.updateVisibleModels();
    $scope.$apply();
    var addBoxBtnDomElem = element.find('#insert-entity-box');
    var addBoxBtn = angular.element(addBoxBtnDomElem);
    addBoxBtn.triggerHandler('mousedown');
    expect($scope.addModel).toHaveBeenCalled();
    expect($scope.addModel.calls.mostRecent().args[0].modelPath).toBe('box');
  });

  it('should update visible models when toggling category', function() {
    spyOn($scope, 'updateVisibleModels');

    $scope.toggleVisibleCategory({ visible: true });

    expect($scope.updateVisibleModels).toHaveBeenCalled();
  });

  it('should generate the robots models from the proxy call', function() {
    $scope.generateRobotsModels().then(res =>
      expect(res[0]).toEqual({
        configPath: 'robot1_name/model.config',
        modelPath: 'robot1_name',
        path: 'robot1_name',
        modelSDF: 'model.sdf',
        modelTitle: 'robot1 name',
        isShared: undefined,
        thumbnail: '<robot png data>',
        custom: true,
        public: false,
        isRobot: true,
        type: 'robots',
        name: 'robot1_name',
        description: 'robot1 description'
      })
    );
    $scope.$digest();
  });

  it('should generate the brain models from the proxy call', function() {
    $scope.generateBrainsModels().then(res =>
      expect(res[0]).toEqual(
        jasmine.objectContaining({
          configPath: undefined,
          path: 'brain1_name',
          modelPath: undefined,
          modelTitle: 'brain1 name',
          thumbnail: undefined,
          custom: false,
          public: true,
          isBrain: true,
          script: undefined,
          description: 'brain1 description'
        })
      )
    );
    $scope.$digest();
  });

  it('should open an error dialog if the file to upload has not the zip extension', function() {
    spyOn(nrpErrorDialog, 'open');
    $scope.uploadModelZip({ type: 'wrong type' }, {});
    $scope.$digest();
    expect(nrpErrorDialog.open).toHaveBeenCalled();
  });

  it('should upload a custom model when past a valid zip and regenerate the models', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn($scope, 'regenerateModels').and.returnValue(window.$q.resolve());
    storageServer.setCustomModel = jasmine
      .createSpy()
      .and.returnValue(window.$q.resolve());
    const entityType = 'robots';
    $scope.uploadModelZip({ type: 'application/zip' }, entityType);
    rootScope.$digest();
    expect(storageServer.getAllModels).toHaveBeenCalledWith(entityType);
    expect(storageServer.setCustomModel).toHaveBeenCalled();
    expect($scope.regenerateModels).toHaveBeenCalled();
    expect($scope.uploadingModel).toBe(false);
  });

  it('should createErrorPopupwhen failing to setCustomModel', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn($scope, 'createErrorPopup').and.returnValue(window.$q.resolve());
    storageServer.setCustomModel = jasmine
      .createSpy()
      .and.returnValue(
        window.$q.reject({ data: 'failed to upload the model' })
      );
    const entityType = 'robots';
    $scope.uploadModelZip({ type: 'application/zip' }, entityType);
    rootScope.$digest();
    expect(storageServer.getAllModels).toHaveBeenCalledWith(entityType);
    expect(storageServer.setCustomModel).toHaveBeenCalled();
    expect($scope.createErrorPopup).toHaveBeenCalled();
    expect($scope.uploadingModel).toBe(false);
  });

  it('should ask to override the model if it already exists', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn(nrpConfirm, 'open').and.returnValue(window.$q.resolve());
    storageServer.setCustomModel = jasmine.createSpy().and.returnValues(
      window.$q.reject({
        data: 'One of your custom models already has the name'
      }),
      window.$q.resolve({ path: 'filename' })
    );
    const entityType = 'robots';
    $scope.uploadModelZip({ type: 'application/zip' }, entityType);
    rootScope.$digest();
    expect(storageServer.getAllModels).toHaveBeenCalledWith(entityType);
    expect(storageServer.setCustomModel).toHaveBeenCalledTimes(2);
    expect(nrpConfirm.open).toHaveBeenCalled();
    expect($scope.uploadingModel).toBe(false);
  });

  it('should call uploadModelZip when uploading a model', () => {
    const inputMock = [];
    inputMock.on = jasmine.createSpy();
    inputMock.click = jasmine.createSpy();
    inputMock.push({ files: [{ type: 'application/zip' }] });
    spyOn(window, '$').and.returnValue(inputMock);
    spyOn(document.body, 'appendChild');
    spyOn(document.body, 'removeChild');

    spyOn($scope, 'uploadModelZip');
    const modelType = 'modelType';
    $scope.uploadModel(modelType);

    expect(window.$).toHaveBeenCalled();
    expect(inputMock.on).toHaveBeenCalledTimes(2);
    expect(inputMock.click).toHaveBeenCalled();
    expect(inputMock.on.calls.count()).toBe(2);
    const uploadCallback = inputMock.on.calls.argsFor(0)[1];
    uploadCallback({ target: { files: [] } });
    expect($scope.uploadModelZip).toHaveBeenCalled();
    const uploadingModelFlagCb = inputMock.on.calls.argsFor(1)[1];
    expect($scope.uploadingModel).toBeFalsy();
    uploadingModelFlagCb();
    expect($scope.uploadingModel).toBe(true);
  });

  it('should call nrpErrorDialog when creating errorPopup', () => {
    spyOn(nrpErrorDialog, 'open');
    const errMsg = 'errMsg';
    $scope.createErrorPopup(errMsg);
    expect(nrpErrorDialog.open).toHaveBeenCalledWith({
      type: 'Error.',
      message: errMsg
    });
  });

  it('should prevent context menu', () => {
    expect(document.addEventListener).toHaveBeenCalledWith(
      'contextmenu',
      jasmine.any(Function)
    );
    const contextmenuCb = document.addEventListener.calls.mostRecent().args[1];
    const eventMock = {
      preventDefault: jasmine.createSpy()
    };
    contextmenuCb(eventMock);
    expect(eventMock.preventDefault).toHaveBeenCalled();
  });

  it('should generate a robot or a brain depending on the category', function() {
    spyOn($scope, 'generateRobotsModels').and.returnValue($q.resolve());
    spyOn($scope, 'generateBrainsModels').and.returnValue($q.resolve());
    $scope
      .generateModel('Robots')
      .then(() => expect($scope.generateRobotsModels).toHaveBeenCalled());
    $scope.$digest();

    $scope
      .generateModel('Brains')
      .then(() => expect($scope.generateBrainsModels).toHaveBeenCalled());
    $scope.$digest();

    $scope.generateRobotsModels.calls.reset();
    $scope.generateBrainsModels.calls.reset();
    $scope.generateModel('Other models').catch(() => {
      expect($scope.generateRobotsModels).not.toHaveBeenCalled();
      expect($scope.generateBrainsModels).not.toHaveBeenCalled();
    });
    $scope.$digest();
  });

  it('should regenerate a model of the Brains or Robots category', function() {
    spyOn($scope, 'generateModel').and.returnValue($q.resolve());
    $scope.categories = [{ title: 'Brains' }, { title: 'Robots' }];
    $scope.regenerateModels().then(() => {
      for (let i = 0; i <= 1; i++)
        expect($scope.generateModel).toHaveBeenCalledWith(
          $scope.categories[i].title
        );
    });
    $scope.$digest();

    $scope.categories = [{ title: 'Broccoli' }];
    $scope.generateModel.calls.reset();
    $scope
      .regenerateModels()
      .then(() => expect($scope.generateModel).not.toHaveBeenCalled());
    $scope.$digest();
  });

  it('should fail to generate the robots models from the proxy call', function() {
    storageServer.getAllModels.and.returnValue($q.reject());
    storageServer.itWasSuccessful = false;
    spyOn(nrpErrorDialog, 'open');

    $scope
      .generateRobotsModels()
      .then(() => expect(nrpErrorDialog.open).toHaveBeenCalled());
    $scope.$digest();
  });

  it('should fail to generate the brains models from the proxy call', function() {
    storageServer.getAllModels.and.returnValue($q.reject());
    storageServer.itWasSuccessful = false;
    spyOn(nrpErrorDialog, 'open');

    $scope
      .generateBrainsModels()
      .then(() => expect(nrpErrorDialog.open).toHaveBeenCalled());
    $scope.$digest();
  });

  it('should fail to generate the KG brain models', function() {
    spyOn($scope, 'generateModels').and.returnValue(
      $q.resolve([{ data: [] }, []])
    );
    storageServer.getKnowledgeGraphBrains = jasmine.createSpy().and.returnValue(
      $q.reject({
        status: '500',
        statusText: 'error'
      })
    );
    spyOn(nrpErrorDialog, 'open');

    $scope
      .generateBrainsModels()
      .then(() => expect(nrpErrorDialog.open).toHaveBeenCalled());
    $scope.$digest();
  });

  it('should add a brain successfully', function() {
    spyOn(nrpConfirm, 'open').and.returnValue($q.resolve());
    storageServer.saveBrain.and.returnValue($q.resolve());

    backendInterfaceService.setBrain.and.returnValue($q.resolve());
    backendInterfaceService.getBrain.and.returnValue($q.resolve());

    $scope.addBrain({
      script: 'testScript',
      modelPath: 'testPath',
      description: 'you wont believe it!'
    });
    rootScope.$digest();
    expect(storageServer.getBrain).toHaveBeenCalled();
    expect(storageServer.saveBrain).toHaveBeenCalled();
    expect(backendInterfaceService.setBrain).toHaveBeenCalled();
    expect(storageServer.saveBrain).toHaveBeenCalled();
    expect(goldenLayoutService.openTool).toHaveBeenCalledWith(
      $scope.TOOL_CONFIGS.BRAIN_EDITOR
    );
  });

  it('should add a brain even though the backend throws', function() {
    spyOn(nrpConfirm, 'open').and.returnValue($q.resolve());
    backendInterfaceService.setBrain.and.returnValue(
      // eslint-disable-next-line camelcase
      $q.reject({ data: { error_message: 'Transfer Function' } })
    );
    storageServer.saveBrain.and.returnValue($q.resolve());
    spyOn(nrpErrorDialog, 'open').and.returnValue($q.resolve());
    backendInterfaceService.getBrain.and.returnValue($q.resolve());

    $scope
      .addBrain({ script: 'testScript', modelPath: 'testPath' })
      .then(() => {
        expect(goldenLayoutService.openTool).toHaveBeenCalledWith(
          $scope.TOOL_CONFIGS.BRAIN_EDITOR
        );
        expect(nrpErrorDialog.open).toHaveBeenCalledWith({
          type: 'Error while setting brain.',
          message:
            'Some of the transfer functions are referencing variables from the old brain script.               Please remove these transfer functions to activate the brain script'
        });
      });
    $scope.$digest();
  });

  it('should not spawn models when in INITIALIZED state', function() {
    spyOn($scope, 'addModel');
    spyOn(window.guiEvents, 'emit');
    $scope.updateVisibleModels();
    $scope.$apply();
    $scope.stateService.currentState = $scope.STATE.INITIALIZED;
    var addBoxBtnDomElem = element.find('#insert-entity-box');
    var addBoxBtn = angular.element(addBoxBtnDomElem);

    addBoxBtn.triggerHandler('mousedown');
    expect($scope.addModel).toHaveBeenCalled();
    expect($scope.addModel.calls.mostRecent().args[0].modelPath).toBe('box');
    expect(window.guiEvents.emit).not.toHaveBeenCalledWith(
      'spawn_entity_start',
      'box'
    );
  });

  it('should execute correctly addModel("box")', function() {
    spyOn(window.guiEvents, 'emit');
    $scope.updateVisibleModels();
    $scope.$apply();
    var addBoxBtnDomElem = element.find('#insert-entity-box');
    var addBoxBtn = angular.element(addBoxBtnDomElem);

    addBoxBtn.triggerHandler('mousedown');

    //should emit 'spawn_entity_start'
    expect(window.guiEvents.emit).toHaveBeenCalledWith(
      'spawn_entity_start',
      'box',
      'box/model.sdf'
    );
  });

  it('should add a callback to the onCreateEntityCallbacks on creating an entity', function(
    done
  ) {
    // Jasmine 1.x does not support async expect checks. So this hack is necessary.
    (async () => {
      const initialListenersCount = $scope.gz3d.gui.guiEvents.listeners(
        'notification_popup'
      ).length;
      let mockModelCreated = {
        name: 'awesome_object'
      };

      let promise = $scope.onEntityCreated(mockModelCreated);

      expect(
        $scope.gz3d.gui.guiEvents.listeners('notification_popup').length
      ).toBeGreaterThan(initialListenersCount);

      fakeCreateEntityFromBackend(mockModelCreated.name);
      await promise;
      done();
    })();
  });

  it('should open the object inspector, select an entity and clean up on creating an entity', function(
    done
  ) {
    // Jasmine 1.x does not support async expect checks. So this hack is necessary.
    (async () => {
      const initialListenersCount = $scope.gz3d.gui.guiEvents.listeners(
        'notification_popup'
      ).length;
      let mockModelCreated = {
        name: 'awesome_object'
      };

      let promise = $scope.onEntityCreated(mockModelCreated);
      fakeCreateEntityFromBackend(mockModelCreated.name);

      await promise;

      expect(goldenLayoutService.openTool).toHaveBeenCalledWith(
        $scope.TOOL_CONFIGS.OBJECT_INSPECTOR
      );
      expect(
        $scope.gz3d.gui.guiEvents.listeners('notification_popup').length
      ).toEqual(initialListenersCount);

      done();
    })();
  });

  it('should open the object inspector, clean up on creating an entity and output a warning when there is no response from the backend', function(
    done
  ) {
    // Jasmine 1.x does not support async expect checks. So this hack is necessary.
    (async () => {
      const initialListenersCount = $scope.gz3d.gui.guiEvents.listeners(
        'notification_popup'
      ).length;
      let mockModelCreated = {
        name: 'awesome_object'
      };
      console.warn = jasmine.createSpy('warn');

      let promise = $scope.onEntityCreated(mockModelCreated);
      await promise;

      expect(console.warn).toHaveBeenCalled();
      expect(goldenLayoutService.openTool).toHaveBeenCalledWith(
        $scope.TOOL_CONFIGS.OBJECT_INSPECTOR
      );
      expect(
        $scope.gz3d.gui.guiEvents.listeners('notification_popup').length
      ).toEqual(initialListenersCount);

      done();
    })();
  });

  it('should create a new dummy anchor and click it when exporting the environment', function() {
    var exportSpy = jasmine.createSpy('export');
    simulationSDFWorld.and.callFake(function() {
      return {
        export: exportSpy.and.callFake(function(args, cb) {
          cb({ sdf: 'dummysdf' });
        })
      };
    });

    var dummyAnchorElement = {
      style: {},
      click: jasmine.createSpy('click')
    };

    spyOn(document, 'createElement').and.callFake(function() {
      return dummyAnchorElement;
    });

    spyOn(document.body, 'appendChild');
    spyOn(document.body, 'removeChild');

    $scope.exportSDFWorld();
    expect(exportSpy).toHaveBeenCalled();
    expect(document.body.appendChild).toHaveBeenCalled();
    expect(document.body.removeChild).toHaveBeenCalled();
    expect(dummyAnchorElement.click).toHaveBeenCalled();
  });

  it('should emit delete_entity and toggle menu when deleteModel is called', function() {
    spyOn($scope.gz3d.gui.guiEvents, 'emit').and.callThrough();
    $scope.deleteModel(); //call function
    //should emit 'delete_entity'
    expect($scope.gz3d.gui.guiEvents.emit).toHaveBeenCalledWith(
      'delete_entity'
    );
  });

  it('should correctly saveSDFIntoCollabStorage', function() {
    let saveSpy = jasmine.createSpy('saveSpy').and.callFake(() => {});
    simulationSDFWorld.and.returnValue({
      save: saveSpy
    });

    expect($scope.isSavingToCollab).toEqual(false);
    $scope.saveSDFIntoCollabStorage();
    expect(saveSpy).toHaveBeenCalledWith(
      { simId: simulationInfo.simulationID },
      {},
      jasmine.any(Function),
      jasmine.any(Function)
    );
    expect($scope.isSavingToCollab).toEqual(true);
    saveSpy.calls.argsFor(0)[2]();
    expect($scope.isSavingToCollab).toBe(false);
    $scope.isSavingToCollab = true;
    spyOn(nrpErrorDialog, 'open');
    saveSpy.calls.argsFor(0)[3]();
    expect($scope.isSavingToCollab).toBe(false);
    expect(nrpErrorDialog.open).toHaveBeenCalled();
  });

  it('should returns category visibility', () => {
    $scope.categories = [
      { title: 'Brains', visible: true },
      { title: 'Robots', visible: false }
    ];

    expect($scope.isCategoryVisible('Brains')).toBe(true);
  });
});

describe('Directive: environment-designer robots models', function() {
  var $scope,
    backendInterfaceService,
    gz3d,
    httpBackend,
    stateService,
    storageServer,
    environmentService,
    $q;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('exd.templates'));
  beforeEach(module('currentStateMockFactory'));
  beforeEach(module('simulationInfoMock'));
  beforeEach(module('stateServiceMock'));
  beforeEach(module('dynamicViewOverlayServiceMock'));
  beforeEach(module('goldenLayoutServiceMock'));
  beforeEach(module('gz3dMock'));
  beforeEach(module('sceneInfoMock'));

  beforeEach(
    module(function($provide) {
      $provide.value(
        'simulationSDFWorld',
        jasmine.createSpy('simulationSDFWorld').and.callThrough()
      );
      $provide.value('bbpConfig', {
        get: jasmine.createSpy('get').and.returnValue({
          bbpce016: {
            gzweb: {
              assets: 'mock_assets',
              websocket: 'mock_websocket'
            }
          }
        })
      });
      $provide.value('panels', {
        close: jasmine.createSpy('close')
      });

      window.GZ3D.modelList = [];
    })
  );

  beforeEach(
    inject(function(
      $rootScope,
      $compile,
      $document,
      _EDIT_MODE_,
      _STATE_,
      _TOOL_CONFIGS_,
      _backendInterfaceService_,
      _currentStateMockFactory_,
      _gz3d_,
      _stateService_,
      _panels_,
      _simulationSDFWorld_,
      _simulationInfo_,
      _nrpErrorDialog_,
      _environmentService_,
      _goldenLayoutService_,
      _$httpBackend_,
      _storageServer_,
      _$q_
    ) {
      $scope = $rootScope.$new();
      $scope.EDIT_MODE = _EDIT_MODE_;
      $scope.STATE = _STATE_;
      $scope.TOOL_CONFIGS = _TOOL_CONFIGS_;

      backendInterfaceService = _backendInterfaceService_;
      gz3d = _gz3d_;
      httpBackend = _$httpBackend_;
      stateService = _stateService_;
      storageServer = _storageServer_;
      environmentService = _environmentService_;
      $q = _$q_;
      var modelLibraryMock = [
        {
          title: 'Shapes',
          thumbnail: 'shapes.png',
          models: [
            {
              modelPath: 'box',
              modelTitle: 'Box',
              thumbnail: 'img/esv/objects/box.png'
            },
            {
              modelPath: 'sphere',
              modelTitle: 'Sphere',
              thumbnail: 'img/esv/objects/sphere.png'
            },
            {
              modelPath: 'cylinder',
              modelTitle: 'Cylinder',
              thumbnail: 'img/esv/objects/cylinder.png',
              physicsIgnore: 'opensim'
            }
          ]
        },
        {
          title: 'Lights',
          thumbnail: 'lights.png',
          models: [
            {
              modelPath: 'pointlight',
              modelTitle: 'Point Light',
              thumbnail: 'img/esv/objects/pointlight.png'
            },
            {
              modelPath: 'spotlight',
              modelTitle: 'Spot Light',
              thumbnail: 'img/esv/objects/spotlight.png'
            },
            {
              modelPath: 'directionallight',
              modelTitle: 'Directional Light',
              thumbnail: 'img/esv/objects/directionallight.png',
              physicsIgnore: 'bullet'
            }
          ]
        }
      ];
      httpBackend.whenGET(/.*assets.*/).respond(modelLibraryMock);

      $compile('<environment-designer />')($scope);
      spyOn(environmentService, 'isDevMode').and.returnValue(true);
      spyOn(storageServer, 'getAllModels').and.returnValue(
        $q.resolve([
          {
            name: 'custom robot1 name',
            description: 'custom robot1 description',
            thumbnail: '<robot png data>',
            id: 'robot1',
            type: 'robots',
            path: 'foldername'
          }
        ])
      );
      spyOn(storageServer, 'getKnowledgeGraphBrains').and.returnValue(
        $q.resolve([
          {
            name: 'custom brain1 name',
            description: 'custom brain1 description',
            thumbnail: '<robot png data>',
            id: 'brain1',
            urls: {
              fileLoader: 'script.py'
            }
          }
        ])
      );
      $scope.$digest();
      httpBackend.flush();
    })
  );

  it(' - addRobot()', function() {
    spyOn(backendInterfaceService, 'addRobot');

    let robotModel = {
      custom: false,
      description: 'description',
      isRobot: true,
      isShared: false,
      modelPath: undefined,
      modelSDF: 'model.sdf',
      modelTitle: 'hbp_clearpath_robotics_husky_a200',
      name: 'hbp_clearpath_robotics_husky_a200',
      path: 'husky_model',
      public: undefined,
      thumbnail: 'thumbnail',
      type: 'robots'
    };

    // call with invalid state
    stateService.currentState = $scope.STATE.INITIALIZED;
    $scope.addRobot(robotModel);
    expect(gz3d.scene.spawnModel.start).not.toHaveBeenCalled();
    expect(backendInterfaceService.addRobot).not.toHaveBeenCalled();

    // valid state, no custom model
    stateService.currentState = $scope.STATE.STARTED;
    let obj = {
      name: 'my_robot_scene_name',
      position: { x: 1, y: 2, z: 3 },
      rotation: { x: 4, y: 5, z: 6 }
    };
    gz3d.iface.addOnCreateEntityCallbacks.and.callFake(callback => {
      callback(obj);
    });
    gz3d.iface.onCreateEntityCallbacks = [];
    let diffObj = angular.copy(obj);
    diffObj.name = 'a_different_name'; // Test branching condition 'if(model.name !== robotID)'
    gz3d.scene.spawnModel.start.and.callFake(
      (modelPath, modelSDF, modelTitle, callback) => {
        callback(diffObj);
      }
    );
    $scope.addRobot(robotModel);
    expect(backendInterfaceService.addRobot).toHaveBeenCalledWith(
      jasmine.any(String),
      jasmine.any(String),
      jasmine.any(String),
      jasmine.any(Object),
      'False'
    );
    // custom model
    robotModel.custom = true;
    gz3d.scene.spawnModel.start.and.callFake(
      (modelPath, modelSDF, modelTitle, callback) => {
        callback(obj);
      }
    );
    $scope.addRobot(robotModel);
    expect(backendInterfaceService.addRobot).toHaveBeenCalledWith(
      jasmine.any(String),
      jasmine.any(String),
      jasmine.any(String),
      jasmine.any(Object),
      'True'
    );
  });

  it(' - onModelMouseDown(), robot', function() {
    spyOn($scope, 'addRobot');
    spyOn(backendInterfaceService, 'getRobot').and.returnValue({
      then: callback => {
        callback();
      }
    });

    let robotModel = {
      modelPath: 'my_robot',
      modelSDF: 'model.sdf',
      modelTitle: 'MyRobot',
      custom: false,
      path: undefined,
      isRobot: true
    };

    // No custom robot
    let event = { preventDefault: jasmine.createSpy('preventDefault') };
    $scope.onModelMouseDown(event, robotModel);
    expect($scope.addRobot).toHaveBeenCalledWith(robotModel);

    // Custom robot
    robotModel.custom = true;
    robotModel.path = 'path/to/zip';
    $scope.onModelMouseDown(event, robotModel);
    expect(backendInterfaceService.getRobot).toHaveBeenCalled();
    expect($scope.addRobot).toHaveBeenCalledWith(robotModel);

    // Not a robot nor a brain
    $scope.addRobot.calls.reset();
    spyOn($scope, 'addModel').and.callThrough();
    robotModel.isRobot = false;
    $scope.stateService.currentState = $scope.STATE.INITIALIZED;
    $scope.onModelMouseDown(event, robotModel);
    expect($scope.addRobot).not.toHaveBeenCalled();
    expect($scope.addModel).toHaveBeenCalled();
  });

  it(' - onModelMouseDown(), brain', function() {
    spyOn($scope, 'addBrain');
    let brainModel = {
      modelPath: 'brain_model/path_to_brain_file',
      modelTitle: 'nut',
      isBrain: true
    };
    let event = { preventDefault: jasmine.createSpy('preventDefault') };
    $scope.onModelMouseDown(event, brainModel);
    expect($scope.addBrain).toHaveBeenCalledWith(brainModel);
  });
});
