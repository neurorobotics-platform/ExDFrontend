'use strict';

describe('Controller: UploadToKgExplorerController', function() {
  beforeEach(module('exdFrontendApp'));
  beforeEach(module('exd.templates'));

  let $rootScope, element, storageServer;
  let $q;

  const MOCKED_DATA = {
    experiments: [
      {
        uuid: '758096b6-e500-452b-93e1-ba2bba203844',
        name: 'New experiment',
        parent: '89857775-6215-4d53-94ee-fb6c18b9e2f8'
      }
    ],
    experimentFiles: [
      {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac12a',
        name: 'all_neurons_spike_monitor.py',
        parent: '758096b6-e500-452b-93e1-ba2bba203844',
        contentType: 'text/plain',
        type: 'file',
        modifiedOn: '2017-08-07T07:59:35.837002Z',
        extension: 'py'
      },
      {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac12c',
        name: 'all_spikes.csv',
        parent: '758096b6-e500-452b-93e1-ba2bba203844',
        contentType: 'text/plain',
        type: 'file',
        modifiedOn: '2017-08-07T07:59:35.837002Z',
        extension: 'csv'
      },
      {
        uuid: '758096b6-e500-452b-93e1-ba2bba203844',
        name: 'csv_records_2019-09-10_13-52-06',
        parent: '758236b6-e500-452b-93e1-ba2bba20323',
        type: 'folder',
        modifiedOn: '2017-08-07T07:59:35.837002Z'
      },
      {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac12b',
        name: 'csv_records_2019-08-30_15-50-16',
        parent: '758236b6-e500-452b-93e1-ba2bba20323',
        type: 'folder',
        modifiedOn: '2017-08-07T07:59:35.837002Z'
      },
      {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac13b',
        name: 'resources',
        parent: '758236b6-e500-452b-93e1-ba2bba20323',
        type: 'folder',
        modifiedOn: '2017-08-07T07:59:35.837002Z'
      }
    ]
  };

  beforeEach(
    inject(function(
      $controller,
      $compile,
      _$rootScope_,
      _storageServer_,
      _$q_
    ) {
      $rootScope = _$rootScope_;
      storageServer = _storageServer_;
      element = $compile('<upload-to-kg-explorer></upload-to-kg-explorer>')(
        $rootScope
      );
      $q = _$q_;
    })
  );

  const checkAndLoadCsvFolder = function() {
    spyOn(storageServer, 'getExperimentFiles').and.returnValue(
      $q.when(MOCKED_DATA.experimentFiles)
    );
    $rootScope.$digest();
    const controller = element.scope().vm;
    controller.checkAndLoadCsvFolder();
    $rootScope.$digest();
    return controller;
  };

  it('should retrieve csv files from latest records folder', function() {
    const controller = checkAndLoadCsvFolder();

    expect(controller.selectedParent.files).toContain(
      jasmine.objectContaining(MOCKED_DATA.experimentFiles[1])
    );
    expect(controller.selectedParent.files).not.toContain(
      jasmine.objectContaining(MOCKED_DATA.experimentFiles[0])
    );
    expect(controller.selectedParent.uuid).toEqual(
      MOCKED_DATA.experimentFiles[2].uuid
    );
  });

  it('should select and unselect a file', function() {
    const controller = checkAndLoadCsvFolder();

    controller.selectFile(MOCKED_DATA.experimentFiles[1].uuid);
    expect(controller.selectedFilesIds).toContain(
      MOCKED_DATA.experimentFiles[1].uuid
    );

    controller.selectFile(MOCKED_DATA.experimentFiles[1].uuid);
    expect(controller.selectedFilesIds).not.toContain(
      MOCKED_DATA.experimentFiles[1].uuid
    );
  });
});
