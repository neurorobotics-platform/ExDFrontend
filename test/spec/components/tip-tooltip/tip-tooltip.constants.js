'use strict';

describe('Constants: tip-constants', function() {
  beforeEach(function() {
    module('tipTooltipModule');
  });

  var DEFAULT_TIP_CLUSTER_IDENTIFIER;

  beforeEach(
    inject(function(_DEFAULT_TIP_CLUSTER_IDENTIFIER_) {
      DEFAULT_TIP_CLUSTER_IDENTIFIER = _DEFAULT_TIP_CLUSTER_IDENTIFIER_;
    })
  );

  it('default should be "general"', function() {
    expect(DEFAULT_TIP_CLUSTER_IDENTIFIER).toBe('general');
  });
});
