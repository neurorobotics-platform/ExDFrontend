'use strict';

describe('Service: tip-service', function() {
  beforeEach(module('tipTooltipModule'));
  beforeEach(module('exdFrontendApp'));

  var tipTooltipService, DEFAULT_TIP_CLUSTER_IDENTIFIER;

  let hideTipAndMarkAsReadMock = tip => {
    tip.hidden = true;
    tip.markedAsRead = true;
  };

  beforeEach(
    inject(function(_tipTooltipService_, _DEFAULT_TIP_CLUSTER_IDENTIFIER_) {
      localStorage.clear();
      tipTooltipService = _tipTooltipService_;
      tipTooltipService.reset();
      DEFAULT_TIP_CLUSTER_IDENTIFIER = _DEFAULT_TIP_CLUSTER_IDENTIFIER_;
    })
  );

  it('should be visible by default ', function() {
    expect(tipTooltipService.hidden).toBe(false);
  });

  it('should set its hide property', function() {
    tipTooltipService.setHidden(true);
    expect(tipTooltipService.hidden).toBe(true);
    tipTooltipService.setHidden(false);
    expect(tipTooltipService.hidden).toBe(false);
  });

  it('should initialize the default tip cluster', function() {
    expect(
      tipTooltipService.tipClusters.hasOwnProperty(
        DEFAULT_TIP_CLUSTER_IDENTIFIER
      )
    ).toBe(true);
  });

  it('should initialize new tip clusters if required', function() {
    tipTooltipService.initializeTipClusterIfNotYetExistent('test');
    expect(tipTooltipService.tipClusters.hasOwnProperty('test')).toBe(true);
  });

  it('should set all markedAsRead properties to false on first creation', function() {
    _.forEach(tipTooltipService.tipCodes, function(tip) {
      expect(tip.markedAsRead).toBe(false);
    });
  });

  it('should reset itself correctly', function() {
    tipTooltipService.initializeTipClusterIfNotYetExistent('test');
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME, 'test');
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.tipClusters['test']).not.toBe(undefined);
    expect(tipTooltipService.tipClusters['test'].tips.length).toBe(1);
    expect(
      tipTooltipService.tipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);

    tipTooltipService.reset();

    expect(tipTooltipService.tipClusters['test']).toBe(undefined);
    expect(
      tipTooltipService.tipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(0);
  });

  it('should set current tip on default cluster', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);
    expect(
      tipTooltipService.tipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips[0]
    ).toBe(tipTooltipService.tipCodes.WELCOME);
  });

  it('should set current tip on specified cluster', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME, 'test');
    expect(tipTooltipService.tipClusters['test'].tips[0]).toBe(
      tipTooltipService.tipCodes.WELCOME
    );
  });

  it('should reset hidden property if the tip is set but already present', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME, 'test');
    tipTooltipService.tipClusters['test'].tips[0].hidden = true;
    expect(tipTooltipService.tipClusters['test'].tips[0].hidden).toBe(true);
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME, 'test');
    expect(tipTooltipService.tipClusters['test'].tips[0].hidden).toBe(false);
  });

  it('should reset tip list position when set current tip', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);
    expect(tipTooltipService.tipCodes.WELCOME.tipListPos).toBe(0);
  });

  it('should not stack duplicate', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);

    expect(
      tipTooltipService.tipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(2);
  });

  it('should reset tip code tips correctly', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME, 'test');
    tipTooltipService.setCurrentTip(
      tipTooltipService.tipCodes.SIMULATIONS_TIPS,
      'test'
    );
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.tipCodes.WELCOME.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.WELCOME.markedAsRead).toBe(false);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.markedAsRead).toBe(
      false
    );
    expect(tipTooltipService.tipCodes.NAVIGATION.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.NAVIGATION.markedAsRead).toBe(false);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(
      undefined
    );
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(
      false
    );

    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.WELCOME);
    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.NAVIGATION);
    tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead = true;
    tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden = true;

    expect(tipTooltipService.tipCodes.WELCOME.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.WELCOME.markedAsRead).toBe(true);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.markedAsRead).toBe(
      false
    );
    expect(tipTooltipService.tipCodes.NAVIGATION.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.NAVIGATION.markedAsRead).toBe(true);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(
      true
    );

    tipTooltipService.resetAllTipCodeTipPreferences();

    expect(tipTooltipService.tipCodes.WELCOME.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.WELCOME.markedAsRead).toBe(false);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.SIMULATIONS_TIPS.markedAsRead).toBe(
      false
    );
    expect(tipTooltipService.tipCodes.NAVIGATION.hidden).toBe(false);
    expect(tipTooltipService.tipCodes.NAVIGATION.markedAsRead).toBe(false);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(
      false
    );
  });

  it('should not set hidden to true on reset when corresponding flag is set', function() {
    expect(
      tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.keepHiddenPropertyOnReset
    ).toBe(true);

    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO);

    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(
      true
    );

    tipTooltipService.resetAllTipCodeTipPreferences();

    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(true);
    expect(tipTooltipService.tipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(
      false
    );
  });

  it('should return correct tip types', function() {
    expect(tipTooltipService.tipToType({ test: ['testtest'] })).toBe('');
    expect(
      tipTooltipService.tipToType(tipTooltipService.tipCodes.WELCOME)
    ).toBe('WELCOME');
  });

  it('should indicate correctly if a tip should be displayed', function() {
    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.hidden = false;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(true);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.hidden = false;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(true);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.hidden = false;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(false);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.hidden = false;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(true);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.hidden = true;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(false);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.hidden = true;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(false);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.hidden = true;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(false);

    tipTooltipService.tipCodes.WELCOME.markedAsRead = true;
    tipTooltipService.tipCodes.WELCOME.doNotShowAgainWhenMarkedAsRead = false;
    tipTooltipService.tipCodes.WELCOME.hidden = true;

    expect(
      tipTooltipService.displayTip(tipTooltipService.tipCodes.WELCOME)
    ).toBe(false);
  });

  it('should support someTipsAreNotDisplayedInTipCluster', function() {
    expect(tipTooltipService.someTipsAreNotDisplayedInTipCluster()).toBe(false);

    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);

    expect(tipTooltipService.someTipsAreNotDisplayedInTipCluster()).toBe(false);

    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.someTipsAreNotDisplayedInTipCluster()).toBe(false);

    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.someTipsAreNotDisplayedInTipCluster()).toBe(true);

    tipTooltipService.resetAllTipCodeTipPreferences();

    expect(tipTooltipService.someTipsAreNotDisplayedInTipCluster()).toBe(false);
  });

  it('should support someTipsAreDisplayedInTipCluster', function() {
    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(false);

    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.WELCOME);

    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(true);

    tipTooltipService.setCurrentTip(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(true);

    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.NAVIGATION);

    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(true);

    hideTipAndMarkAsReadMock(tipTooltipService.tipCodes.WELCOME);

    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(false);

    tipTooltipService.resetAllTipCodeTipPreferences();

    expect(tipTooltipService.someTipsAreDisplayedInTipCluster()).toBe(true);
  });
});
