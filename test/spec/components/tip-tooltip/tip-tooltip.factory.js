'use strict';

describe('Factory: tip-tooltip factories', function() {
  beforeEach(() => {
    module('tipTooltipModule');
    module('exdFrontendApp');
  });

  var tipCodesFactory, tipClustersFactory;

  beforeEach(
    inject(function(_tipCodesFactory_, _tipClustersFactory_) {
      tipCodesFactory = _tipCodesFactory_;
      tipClustersFactory = _tipClustersFactory_;
    })
  );

  it('tip codes have a valid structure', function() {
    let validTipProperties = [
      'tipList',
      'text',
      'image',
      'doNotShowAgainWhenMarkedAsRead',
      'keepHiddenPropertyOnReset'
    ];

    let validTipListProperties = ['text', 'image'];

    let tipCodes = tipCodesFactory.getTipCodes();

    _.forEach(tipCodes, function(tip) {
      expect(tip.text !== undefined || tip.tipList !== undefined).toBe(true);

      let keys = Object.keys(tip);

      // Check for tipLists and validate them.
      if (keys.includes('tipList')) {
        _.forEach(tip.tipList, function(tipListEntry) {
          expect(tipListEntry.text !== undefined).toBe(true);

          let tipListKeys = Object.keys(tipListEntry);
          tipListKeys = tipListKeys.filter(
            value => validTipListProperties.includes(value) === false
          );
          expect(tipListKeys.length).toBe(0);
        });
      }

      // Check if there ae not any invalid first order properties.
      keys = keys.filter(value => validTipProperties.includes(value) === false);
      expect(keys.length).toBe(0);
      if (keys.length > 0) {
        console.log(keys);
      }
    });
  });

  it('tip clusters return a empty object', function() {
    let tipClusters = tipClustersFactory.getTipClusters();

    expect(Object.keys(tipClusters).length).toBe(0);
    expect(tipClusters.constructor).toBe(Object);
  });
});
