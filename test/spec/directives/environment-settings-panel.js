/* global GZ3D: true */
'use strict';

describe('Directive: 3d settings', function() {
  var scope, gz3d, collab3DSettingsService, environmentService;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('exd.templates'));

  beforeEach(module('gz3dMock'));
  beforeEach(module('simulationInfoMock'));
  beforeEach(module('userInteractionSettingsServiceMock'));
  beforeEach(module('environmentServiceMock'));

  beforeEach(
    module(function($provide) {
      collab3DSettingsService = {
        saveSettings: jasmine.createSpy('saveSettings'),
        setDefaultNavigationSensitivity: jasmine.createSpy(
          'setDefaultNavigationSensitivity'
        )
      };

      GZ3D.MASTER_QUALITY_BEST = 'Best';
      GZ3D.MASTER_QUALITY_MIDDLE = 'Middle';
      GZ3D.MASTER_QUALITY_LOW = 'Low';
      GZ3D.MASTER_QUALITY_MINIMAL = 'Minimal';

      $provide.value('collab3DSettingsService', collab3DSettingsService);
    })
  );

  beforeEach(
    inject(function(
      $rootScope,
      $compile,
      $httpBackend,
      SKYBOX_LIBRARY,
      _gz3d_,
      _environmentService_
    ) {
      gz3d = _gz3d_;
      environmentService = _environmentService_;

      var element = $compile('<environment-settings-panel/>')($rootScope);
      var regex = new RegExp('.*' + SKYBOX_LIBRARY);
      $httpBackend.whenGET(regex).respond({ skyList: [], skyLabelList: [] });
      $httpBackend.whenGET('http://proxy/identity').respond(200);

      scope = element.scope();
      $rootScope.$digest();

      $httpBackend.flush();

      environmentService.isPrivateExperiment.and.returnValue(true);
    })
  );

  it('should reapply composer settings on resetSettings', function() {
    scope.resetSettings();
    expect(gz3d.scene.applyComposerSettings).toHaveBeenCalled();
  });

  it('should save 3d setting on environment save', function() {
    scope.saveSettings();
    expect(collab3DSettingsService.saveSettings).toHaveBeenCalled();
  });

  it('should register deinitialize callback with gz3d service', function() {
    spyOn(scope, 'saveSettings');
    expect(gz3d.onDeinitialize).toHaveBeenCalled();
    // call the callback
    let deinitCallback = gz3d.onDeinitialize.calls.mostRecent().args[0];
    deinitCallback();
    expect(scope.saveSettings).toHaveBeenCalled();
  });
});
