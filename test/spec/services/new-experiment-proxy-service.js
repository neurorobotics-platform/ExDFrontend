'use strict';

describe('Services: new-experiment-proxy-service', function() {
  let bbpConfig, newExperimentProxyService, proxyUrl, storageUrl;
  // load the service to test and mock the necessary service
  beforeEach(module('exdFrontendApp'));
  beforeEach(module('exd.templates'));
  beforeEach(
    inject(function(_bbpConfig_, _newExperimentProxyService_) {
      bbpConfig = _bbpConfig_;
      newExperimentProxyService = _newExperimentProxyService_;
      proxyUrl = bbpConfig.get('api.proxy.url');
      storageUrl = `${proxyUrl}/storage`;
    })
  );

  it('should make sure the get proxy url returns the correct url', function() {
    spyOn(bbpConfig, 'get').and.callThrough();
    const testProxyUrl = newExperimentProxyService.getProxyUrl();
    expect(bbpConfig.get).toHaveBeenCalledWith('api.proxy.url');
    expect(testProxyUrl).toEqual(proxyUrl);
  });

  it('should make sure the get storage url returns the correct url', function() {
    spyOn(bbpConfig, 'get').and.callThrough();
    const testStorageUrl = newExperimentProxyService.getStorageUrl();
    expect(testStorageUrl).toEqual(storageUrl);
  });
});
