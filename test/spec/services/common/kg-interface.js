(function() {
  'use strict';

  describe('Service: kgInterface', function() {
    let kgInterfaceService, $httpBackend;

    beforeEach(module('kgInterfaceService'));

    beforeEach(
      inject(function(_$httpBackend_, _kgInterfaceService_) {
        $httpBackend = _$httpBackend_;
        kgInterfaceService = _kgInterfaceService_;
      })
    );

    it('should build KG interface resource', function() {
      kgInterfaceService.buildKgInterfaceResource();

      expect(kgInterfaceService.kgRsc).toBeDefined();
    });

    it('should upload a simulation instance', function() {
      const simulation = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
        'nxv:rev': 1
      };

      $httpBackend
        .expectPOST(/core\/simulation\/v1.0.0/)
        .respond(200, simulation);
      kgInterfaceService.postKgSimulation('body').then(res => {
        expect(res.toJSON()).toEqual(simulation);
      });
      $httpBackend.flush();
    });

    it('should update a simulation instance', function() {
      const simulation = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
        'nxv:rev': 1
      };

      $httpBackend
        .expectPUT(/core\/simulation\/v1.0.0\/id/)
        .respond(200, simulation);
      kgInterfaceService.updateKgSimulation('id', 1, 'body').then(res => {
        expect(res.toJSON()).toEqual(simulation);
      });
      $httpBackend.flush();
    });

    it('should upload an artifact instance', function() {
      const artifact = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
        'nxv:rev': 1
      };

      $httpBackend.expectPOST(/artifacts\/type\/v1.0.0/).respond(200, artifact);
      kgInterfaceService.postKgArtifact('type', 'body').then(res => {
        expect(res.toJSON()).toEqual(artifact);
      });
      $httpBackend.flush();
    });

    it('should update an artifact instance', function() {
      const artifact = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
        'nxv:rev': 1
      };

      $httpBackend
        .expectPUT(/artifacts\/type\/v1.0.0\/id/)
        .respond(200, artifact);
      kgInterfaceService.updateKgArtifact('type', 'id', 1, 'body').then(res => {
        expect(res.toJSON()).toEqual(artifact);
      });
      $httpBackend.flush();
    });
  });
})();
