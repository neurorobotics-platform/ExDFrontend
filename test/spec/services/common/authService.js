(function() {
  'use strict';

  describe('Service: AuthService', function() {
    beforeEach(module('authModule'));

    let keycloakMock = {
      authenticated: false,
      init: jasmine.createSpy('init').and.returnValue(Promise.resolve()),
      login: jasmine.createSpy('login').and.returnValue(Promise.resolve()),
      logout: jasmine.createSpy('logout').and.returnValue(Promise.resolve()),
      clearStoredLocalToken: jasmine.createSpy('clearStoredLocalToken'),
      updateToken: jasmine
        .createSpy('updateToken')
        .and.returnValue(Promise.resolve()),
      loadUserInfo: jasmine
        .createSpy('loadUserInfo')
        .and.returnValue(Promise.resolve())
    };
    beforeEach(() => {
      window.Keycloak = () => {
        return keycloakMock;
      };
    });

    let windowMock = {
      location: {
        href: undefined
      }
    };

    let bbpConfigMock = {
      api: {
        proxy: {
          url: 'test-auth-proxy-url'
        }
      },
      auth: {
        enableOIDC: false,
        clientId: 'test-auth-client-id',
        url: 'test-auth-url'
      }
    };
    bbpConfigMock.get = jasmine.createSpy('get').and.callFake(string => {
      if (string === 'api.proxy.url') return bbpConfigMock.api.proxy.url;
      else if (string === 'auth') return bbpConfigMock.auth;
      else if (string === 'auth.enableOIDC')
        return bbpConfigMock.auth.enableOIDC;
    });

    let locationMock = {
      url: jasmine.createSpy('get')
    };

    beforeEach(
      module(function($provide) {
        $provide.value('$window', windowMock);
        $provide.value('$location', locationMock);
        $provide.value('bbpConfig', bbpConfigMock);
      })
    );

    let $rootScope;
    let authService;
    beforeEach(
      inject(function(_$rootScope_, _authService_) {
        $rootScope = _$rootScope_;
        authService = _authService_;
      })
    );

    it(' - init()', function() {
      spyOn(authService, 'authCollab').and.returnValue(Promise.resolve());
      spyOn(authService, 'checkForNewLocalTokenToStore');

      // local mode
      authService.oidcEnabled = false;
      authService.init();
      authService.promiseInitialized.then(() => {
        expect(authService.checkForNewLocalTokenToStore).toHaveBeenCalled();
      });

      // collab mode
      authService.oidcEnabled = true;
      authService.init();
      authService.promiseInitialized.then(() => {
        expect(authService.authCollab).toHaveBeenCalled();
      });
    });

    it(
      ' - init(), failing to initialize',
      function(done) {
        spyOn(authService, 'authCollab').and.returnValue(Promise.reject());

        authService.oidcEnabled = true;
        authService.init();
        authService.promiseInitialized.catch(() => {
          expect(authService.initialized).toBe(false);
          done();
        });
      },
      30000
    );

    it(' - authenticate()', function() {
      spyOn(authService, 'authCollab');
      spyOn(authService, 'authLocal');

      // local mode
      authService.oidcEnabled = false;
      authService.authenticate();
      expect(authService.authLocal).toHaveBeenCalled();

      // collab mode
      authService.oidcEnabled = true;
      authService.authenticate();
      expect(authService.authCollab).toHaveBeenCalled();
    });

    it(' - getToken()', function(done) {
      spyOn(authService, 'getStoredLocalToken');
      spyOn(console, 'error');

      // local mode
      authService.oidcEnabled = false;
      authService.getToken();
      expect(authService.getStoredLocalToken).toHaveBeenCalled();

      // collab mode, no authentication
      authService.oidcEnabled = true;
      authService.getToken();
      expect(console.error).toHaveBeenCalled();
      // collab mode, with authentication
      keycloakMock.authenticated = true;
      keycloakMock.token = 'test-auth-keycloak-token';
      authService.initKeycloakClient();
      let token = authService.getToken();
      $rootScope.$digest();
      expect(keycloakMock.updateToken).toHaveBeenCalled();
      expect(token).toBe(keycloakMock.token);
      // collab mode, rejected updateToken
      keycloakMock.updateToken.and.returnValue(Promise.reject());
      console.error.calls.reset();
      token = authService.getToken();
      //$rootScope.$digest();
      setTimeout(() => {
        expect(console.error).toHaveBeenCalled();
        done();
      }, 10);
    });

    it(' - logout()', function() {
      spyOn(authService, 'clearStoredLocalToken');
      spyOn(console, 'error');

      // local mode
      authService.oidcEnabled = false;
      authService.logout();
      expect(authService.clearStoredLocalToken).toHaveBeenCalled();

      // collab mode, no authentication
      keycloakMock.authenticated = false;
      authService.oidcEnabled = true;
      authService.logout();
      expect(console.error).toHaveBeenCalled();
      // collab mode, with authentication
      keycloakMock.authenticated = true;
      keycloakMock.token = 'test-auth-keycloak-token';
      authService.initKeycloakClient();
      $rootScope.$digest();
      authService.logout();
      expect(keycloakMock.logout).toHaveBeenCalled();
      expect(keycloakMock.clearStoredLocalToken).toHaveBeenCalled();
    });

    it(' - authLocal(), already authenticating', function() {
      spyOn(localStorage, 'removeItem');

      authService.authenticating = true;
      authService.authLocal();
      expect(localStorage.removeItem).not.toHaveBeenCalled();
      expect(windowMock.location.href).not.toBeDefined();
    });

    it(' - authLocal(), free to authenticate', function() {
      spyOn(localStorage, 'removeItem');

      // free to authenticate
      authService.authenticating = false;
      authService.authLocal();
      expect(localStorage.removeItem).toHaveBeenCalledWith(
        authService.STORAGE_KEY
      );
      expect(windowMock.location.href).toBeDefined();
    });

    it(' - checkForNewLocalTokenToStore()', function() {
      spyOn(localStorage, 'setItem');

      // no access token
      authService.checkForNewLocalTokenToStore();
      expect(localStorage.setItem).not.toHaveBeenCalled();

      let accessToken = 'test-auth-access-token';
      locationMock.url.and.returnValue(
        'http://test.auth.url&access_token=' + accessToken
      );
      authService.checkForNewLocalTokenToStore();
      expect(localStorage.setItem).toHaveBeenCalled();
      expect(locationMock.url).toHaveBeenCalledWith('http://test.auth.url');
    });

    it(' - clearStoredLocalToken()', function() {
      spyOn(localStorage, 'removeItem');

      authService.clearStoredLocalToken();
      expect(localStorage.removeItem).toHaveBeenCalledWith(
        authService.STORAGE_KEY
      );
    });

    it(' - getStoredLocalToken()', function() {
      // no token
      authService.clearStoredLocalToken();
      let token = authService.getStoredLocalToken();
      expect(token).toBe('no-token');

      // with token
      let accessToken = 'test-auth-access-token';
      let sessionState = 'test-auth-session-state';
      locationMock.url.and.returnValue(
        'http://test.auth.url&access_token=' +
          accessToken +
          '&session_state=' +
          sessionState
      );
      authService.checkForNewLocalTokenToStore();
      token = authService.getStoredLocalToken();
      expect(token).toBe(accessToken);

      // malformed token
      localStorage.setItem(authService.STORAGE_KEY, {});
      token = authService.getStoredLocalToken();
      expect(token).toBe('malformed-token');
    });

    it(' - authCollab(), already authenticating', function() {
      spyOn(authService, 'initKeycloakClient');

      authService.authenticating = true;
      authService.authCollab();
      expect(authService.initKeycloakClient).not.toHaveBeenCalled();
    });

    it(' - authCollab(), no ongoing authentication, needs login', function(
      done
    ) {
      spyOn(authService, 'initKeycloakClient').and.callThrough();

      authService.authenticating = false;
      keycloakMock.authenticated = false;
      authService.authCollab().then(() => {
        expect(keycloakMock.login).toHaveBeenCalled();
        done();
      });
    });

    it(' - authCollab(), no ongoing authentication, can retrieve data', function(
      done
    ) {
      spyOn(authService, 'initKeycloakClient').and.callThrough();

      authService.authenticating = false;
      keycloakMock.authenticated = true;
      authService.authCollab().then(() => {
        expect(keycloakMock.loadUserInfo).toHaveBeenCalled();
        done();
      });
    });
  });
})();
