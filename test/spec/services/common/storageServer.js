(function() {
  'use strict';

  describe('Service: storageServer', function() {
    var storageServer, $httpBackend;
    var windowMock = {
      location: {
        href: null
      }
    };
    beforeEach(
      module(function($provide) {
        $provide.value('$window', windowMock);
      })
    );
    beforeEach(module('storageServer'));
    beforeEach(module('authServiceMock'));

    beforeEach(
      inject(function(_$httpBackend_, _storageServer_) {
        $httpBackend = _$httpBackend_;
        storageServer = _storageServer_;
      })
    );

    it('should build storage resource', function() {
      storageServer.buildStorageResource();
      expect(storageServer.proxyRsc).toBeDefined();
    });

    it('should retrieve all customs models', function() {
      var customModels = [
        {
          uuid: 'fileName',
          fileName: 'fileName',
          userId: 'token'
        }
      ];
      $httpBackend.expectGET(/models/).respond(200, customModels);
      storageServer.getModelsbyType().then(function(res) {
        expect(res[0].toJSON()).toEqual(customModels[0]);
      });
      $httpBackend.flush();
    });

    it('should retrieve customs models', function() {
      var customModels = [
        {
          uuid: 'fileName',
          fileName: 'fileName',
          userId: 'token'
        }
      ];
      $httpBackend.expectGET(/models/).respond(200, customModels);
      storageServer.getCustomModelsByUser().then(function(res) {
        expect(res[0].toJSON()).toEqual(customModels[0]);
      });
      $httpBackend.flush();
    });

    it('should retrieve experiments', function() {
      var experiments = ['exp1', 'exp2'];
      $httpBackend
        .expectGET(/storage\/experiments/)
        .respond(200, angular.copy(experiments, []));
      storageServer.getExperiments().then(function(res) {
        expect(res[0]).toBe(experiments[0]);
        expect(res[1]).toBe(experiments[1]);
      });
      $httpBackend.flush();
    });

    //TODO: make part of authService tests
    /*it('should set set access_token in storage when access_token in url', function() {
      spyOn($location, 'url').and.returnValue('/esv-private&access_token=test');
      spyOn(localStorage, 'setItem');
      storageServer.storageServerTokenManager.checkForNewTokenToStore();
      expect(localStorage.setItem).toHaveBeenCalledWith(
        storageServer.storageServerTokenManager.STORAGE_KEY,
        '[{"access_token":"test"}]'
      );
    });*/

    it('should update SharedExperiments Option', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/experiments/sharingmode')
        .respond(200, []);
      storageServer.updateSharedExpMode();
      $httpBackend.flush();
    });

    it('should delete a user the Sharing User list', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/experiments/sharingusers')
        .respond(200, []);
      storageServer.addSharedExpUsers();
      $httpBackend.flush();
    });

    it('should add a user into the Sharing User list ', function() {
      $httpBackend
        .expectDELETE('http://proxy/storage/experiments/sharingusers')
        .respond(200, []);
      storageServer.deleteSharedExpUser();
      $httpBackend.flush();
    });

    it('should get the share option of the experiment ', function() {
      $httpBackend
        .expectGET('http://proxy/storage/experiments/sharingmode')
        .respond(200, {});
      storageServer.getSharedExpMode();
      $httpBackend.flush();
    });

    it('should get the the models by type', function() {
      $httpBackend
        .expectGET('http://proxy/storage/models/fakeModelType')
        .respond(200, []);
      storageServer.getModelsbyType('fakeModelType');
      $httpBackend.flush();
    });

    it('should get all the the models', function() {
      $httpBackend
        .expectGET('http://proxy/storage/models/all/fakeModelType')
        .respond(200, []);
      storageServer.getAllModels('fakeModelType');
      $httpBackend.flush();
    });

    it('should custom models by user', function() {
      $httpBackend
        .expectGET('http://proxy/storage/models/user/fakeModelType')
        .respond(200, []);
      storageServer.getCustomModelsByUser('fakeModelType');
      $httpBackend.flush();
    });

    it('should get custom shared models', function() {
      $httpBackend
        .expectGET('http://proxy/storage/models/shared/fakeModelType')
        .respond(200, []);
      storageServer.getCustomSharedModels('fakeModelType');
      $httpBackend.flush();
    });

    it('should set custom models', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/models/fakeModelType/fakeModelName')
        .respond(200, {});
      storageServer.setCustomModel('fakeModelType', 'fakeModelName');
      $httpBackend.flush();
    });

    it('should get all Users', function() {
      $httpBackend.expectGET('http://proxy/identity/me/users').respond(200, []);
      storageServer.getAllUsers();
      $httpBackend.flush();
    });

    it('should get shared Exp Users', function() {
      $httpBackend
        .expectGET('http://proxy/storage/experiments/fakeExpId/sharingusers')
        .respond(200, []);
      storageServer.getSharedExpUsers('fakeExpId');
      $httpBackend.flush();
    });

    it('should get shared Exp Mode', function() {
      $httpBackend
        .expectGET('http://proxy/storage/experiments/fakeExpId/sharingmode')
        .respond(200, {});
      storageServer.getSharedExpMode('fakeExpId');
      $httpBackend.flush();
    });

    it('should add shared model Users', function() {
      $httpBackend
        .expectPOST(
          'http://proxy/storage/models/fakeModelType/fakemodelId/sharing/fakeUserId'
        )
        .respond(200, []);
      storageServer.addSharedModelUsers(
        'fakeModelType',
        'fakemodelId',
        'fakeUserId'
      );
      $httpBackend.flush();
    });

    it('should delete model sharing User', function() {
      $httpBackend
        .expectDELETE(
          'http://proxy/storage/models/fakeModelType/fakemodelId/sharingusers'
        )
        .respond(200, []);
      storageServer.deleteSharedModelUser('fakeModelType', 'fakemodelId');
      $httpBackend.flush();
    });

    it('should get shared model User', function() {
      $httpBackend
        .expectGET(
          'http://proxy/storage/models/fakeModelType/fakemodelId/sharingusers'
        )
        .respond(200, []);
      storageServer.getSharedModelUsers('fakeModelType', 'fakemodelId');
      $httpBackend.flush();
    });

    it('should get shared model mode', function() {
      $httpBackend
        .expectGET(
          'http://proxy/storage/models/fakeModelType/fakemodelId/sharingmode'
        )
        .respond(200, {});
      storageServer.getSharedModelMode('fakeModelType', 'fakemodelId');
      $httpBackend.flush();
    });

    it('should get update shared model mode', function() {
      $httpBackend
        .expectPOST(
          'http://proxy/storage/models/fakeModelType/fakemodelId/sharingmode'
        )
        .respond(200, []);
      storageServer.updateSharedModelMode('fakeModelType', 'fakemodelId');
      $httpBackend.flush();
    });

    it('should set file content', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/experimentId/fakeFile?byname=false')
        .respond(200, {});
      storageServer.setFileContent('experimentId', 'fakeFile', {});
      $httpBackend.flush();
    });

    it('should set blob content', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/experimentId/fakeFile?byname=false')
        .respond(200, {});
      storageServer.setBlobContent('experimentId', 'fakeFile', {});
      $httpBackend.flush();
    });

    it('should accept GDPR', function() {
      $httpBackend.expectPOST('http://proxy/identity/gdpr').respond(200, {});
      storageServer.acceptGdpr();
      $httpBackend.flush();
    });

    it('should clone cloned experiments', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/clone/experiment')
        .respond(200, {});
      storageServer.cloneClonedExperiment('experiment');
      $httpBackend.flush();
    });

    it('should clone template', function() {
      $httpBackend.expectPOST('http://proxy/storage/clone').respond(200, {});
      storageServer.cloneTemplate('expPath', 'contextId');
      $httpBackend.flush();
    });

    it('should clone new experiment', function() {
      $httpBackend.expectPOST('http://proxy/storage/clonenew').respond(200, {});
      storageServer.cloneNew(
        'environmentPath',
        'contextId',
        'experimentName',
        'experimentMode'
      );
      $httpBackend.flush();
    });

    it('should get maintenance mode', function() {
      $httpBackend.expectGET('http://proxy/maintenancemode').respond(200, {});
      storageServer.getMaintenanceMode();
      $httpBackend.flush();
    });

    it('should import a zip experiment', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/importExperiment')
        .respond(200, {});
      storageServer.importExperiment(new Blob());
      $httpBackend.flush();
    });

    it('should scan the user storage', function() {
      $httpBackend
        .expectPOST('http://proxy/storage/scanStorage')
        .respond(200, {});
      storageServer.scanStorage();
      $httpBackend.flush();
    });

    it('should fetch the experiment related zips', function() {
      $httpBackend
        .expectGET('http://proxy/storage/experiments/experimentId/zip')
        .respond(200, {});
      storageServer.getExperimentZips('experimentId');
      $httpBackend.flush();
    });

    it('should fetch the experiment related zips', function() {
      $httpBackend
        .expectGET('http://proxy/storage/zip?name=zipName&path=zipPath')
        .respond(200, {});
      storageServer.getZip('zipPath', 'zipName');
      $httpBackend.flush();
    });

    it('should reject the promise in demo carousel mode', function() {
      window.bbpConfig.demomode = {
        demoCarousel: true
      };
      expect(storageServer.getRobotConfigPath('uuid')).toEqual(
        Promise.reject()
      );
      window.bbpConfig.demomode = undefined;
    });

    it('should return an empty array when calling the KG', function() {
      expect(storageServer.getKnowledgeGraphBrains('query')).toEqual(
        Promise.resolve([])
      );
    });

    it('should save Brain', function() {
      $httpBackend
        .expectPUT('http://proxy/experiment/experimentId/brain')
        .respond(200, {});
      storageServer.saveBrain(
        'experimentId',
        'brain',
        'populations',
        'removePopulations',
        'newBrain'
      );
      $httpBackend.flush();
    });

    it('should get state machines', function() {
      $httpBackend
        .expectGET('http://proxy/experiment/experimentId/stateMachines')
        .respond(200, {});
      storageServer.getStateMachines('experimentId');
      $httpBackend.flush();
    });

    it('should save state machines', function() {
      $httpBackend
        .expectPUT('http://proxy/experiment/experimentId/stateMachines')
        .respond(200, {});
      storageServer.saveStateMachines('experimentId', 'stateMachines');
      $httpBackend.flush();
    });

    it('should get transfer functions', function() {
      $httpBackend
        .expectGET('http://proxy/experiment/experimentId/transferFunctions')
        .respond(200, {});
      storageServer.getTransferFunctions('experimentId');
      $httpBackend.flush();
    });

    it('should save transfer functions', function() {
      $httpBackend
        .expectPUT('http://proxy/experiment/experimentId/transferFunctions')
        .respond(200, {});
      storageServer.saveTransferFunctions('experimentId', 'transferFunctions');
      $httpBackend.flush();
    });

    it('should put a KG artifact attachment', function() {
      $httpBackend
        .expectPUT(
          'http://proxy.production/storage/knowledgeGraph/data/filename'
        )
        .respond(200, {});
      storageServer.saveKgAttachment('filename', 'body');
      $httpBackend.flush();
    });
  });
})();
